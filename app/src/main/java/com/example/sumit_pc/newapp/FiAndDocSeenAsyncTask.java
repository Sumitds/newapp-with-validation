package com.example.sumit_pc.newapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by sumit-pc on 24/11/17.
 */

public class FiAndDocSeenAsyncTask extends AsyncTask<String,Void, String> {

    String json_getUrl = "";
    String username = "";
    String JSON_STRING = "";
    Context activityContext = null;
    SeenCallBack seenCallBack;
    String type="";
    String caseId="";
    ProgressDialog progressDialog;

    FiAndDocSeenAsyncTask(Context activityContext, SeenCallBack seenCallBack){
        this.activityContext = activityContext;
        this.seenCallBack = seenCallBack;
    }

    @Override
    protected void onPreExecute() {
        json_getUrl = "http://www.nscs.in/api/seen";
        progressDialog = new ProgressDialog(activityContext);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {

        try {
            this.username = params[0];
            this.type = params[1];
            this.caseId = params[2];

            URL loginURL = new URL(json_getUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setAllowUserInteraction(false);
            httpURLConnection.setRequestProperty("username",username);
            httpURLConnection.setRequestProperty("type",type);
            httpURLConnection.setRequestProperty("id",caseId);

            OutputStream OS = httpURLConnection.getOutputStream();
            BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));

            String data = URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(this.username,"UTF-8")+"&"+
                    URLEncoder.encode("type","UTF-8") + "=" + URLEncoder.encode(this.type,"UTF-8")+"&"+
                    URLEncoder.encode("id","UTF-8") + "=" + URLEncoder.encode(this.caseId,"UTF-8");

            BW.write(data);
            BW.flush();
            BW.close();
            OS.close();

            InputStream IS = httpURLConnection.getInputStream();

            Log.d("IS","IS reached");
            BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
            Log.d("BR","BR reached");
            StringBuilder sb = new StringBuilder();

            while ((JSON_STRING = BR.readLine())!=null){
                sb.append(JSON_STRING+"\n");
            }

            String jsn = sb.toString().trim();
            Log.d("jsn",jsn);
            BR.close();
            IS.close();

            httpURLConnection.disconnect();

            return  jsn;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();}
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();
        String responseStatus = "";
        try {
            JSONObject jsonObject = new JSONObject(result);
            responseStatus = jsonObject.getString("Response");
            if(responseStatus.equals("Success")){
                seenCallBack.onSuccess("success"+caseId);
            }else{
                Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
