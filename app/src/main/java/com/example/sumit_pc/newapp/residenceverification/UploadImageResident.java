package com.example.sumit_pc.newapp.residenceverification;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.RequestHandler;
import com.example.sumit_pc.newapp.officeverification.GeneralDetailsFiSubmit;
import com.example.sumit_pc.newapp.officeverification.UploadPhotos;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.R.attr.bitmap;

/**
 * Created by sumit-pc on 6/12/17.
 */

public class UploadImageResident extends AppCompatActivity {

//    public static final String UPLOAD_URL_LOCAL = "http://192.168.0.122/api/photoInsert.php";
    public static final String UPLOAD_URL = "http://www.nscs.in/api/img_submit";

    ImageView img1, img2, img3, img4, img5, img6, img7;
    Button uplaodImages;
    String exception ="";

    Bitmap image1, image2, image3, image4, image5, image6, image7;

    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE_RESIDENT = 200;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE1 = 201;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE2 = 202;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE3 = 203;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE4 = 204;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE5 = 205;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE6 = 206;
    String id,imageUser1,imageUser2,imageUser3,imageUser4,imageUser5,imageUser6,imageUser7;
    String username, JSON_STRING;
    final String uploadPhotos = "UploadPhotosResident";
    SharedPreferences sharedPreferences, getUserSharedPref;
    SharedPreferences.Editor editor;

    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_image_resident);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        img1 = (ImageView) findViewById(R.id.img1_fi_resident);
        img2 = (ImageView) findViewById(R.id.img2_fi_resident);
        img3 = (ImageView) findViewById(R.id.img3_fi_resident);
        img4 = (ImageView) findViewById(R.id.img4_fi_resident);
        img5 = (ImageView) findViewById(R.id.img5_fi_resident);
        img6 = (ImageView) findViewById(R.id.img6_fi_resident);
        img7 = (ImageView) findViewById(R.id.img7_fi_resident);
        uplaodImages = (Button) findViewById(R.id.uplaod_photo_resident);


        byte[] byteArray = getIntent().getByteArrayExtra("image");

        try {
            image1 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imageUser1 = ConvertBitmapToString(image1);
            img1.setImageBitmap(image1);
        }catch (NullPointerException npe){

        }
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadImageResident.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadImageResident.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadImageResident.CAMERA_CAPTURE_IMAGE_REQUEST_CODE1);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE1);
                    // permission has been granted, continue as usual

                }


            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadImageResident.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadImageResident.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE2);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE2);
                    // permission has been granted, continue as usual

                }
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadImageResident.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadImageResident.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE3);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE3);
                    // permission has been granted, continue as usual

                }
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadImageResident.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadImageResident.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE4);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE4);
                    // permission has been granted, continue as usual

                }
            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadImageResident.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadImageResident.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE5);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE5);
                    // permission has been granted, continue as usual

                }
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadImageResident.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadImageResident.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE6);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE6);
                    // permission has been granted, continue as usual

                }
            }
        });

        sharedPreferences = getSharedPreferences(uploadPhotos, Context.MODE_PRIVATE);
        getUserSharedPref = getSharedPreferences("FiDataUser", Context.MODE_PRIVATE);

        uplaodImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = getUserSharedPref.getString("caseId","");
                username = getUserSharedPref.getString("username","");

                editor = sharedPreferences.edit();
                editor.clear();
                editor.putString("caseId", id);
                editor.putString("username",username);
                editor.commit();
//                uploadImage();

                UploadImageResident.UploadImageFiResident uploadImageFiResident = new UploadImageResident.UploadImageFiResident();
                uploadImageFiResident.execute("http://www.nscs.in/api/img_submit");
//                uploadImageFiResident.execute("http://172.26.116.182/api/photoInsert.php");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 201) {
            if (resultCode == RESULT_OK) {
                image2 = (Bitmap) data.getExtras().get("data");
                imageUser2 = ConvertBitmapToString(image2);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image2.compress(Bitmap.CompressFormat.PNG, 100, stream);

                img2.setImageBitmap(image2);
            }else if (resultCode == RESULT_CANCELED)
            {
                Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 202) {
            if (resultCode == RESULT_OK) {
                image3 = (Bitmap) data.getExtras().get("data");
                imageUser3 = ConvertBitmapToString(image3);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image3.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img3.setImageBitmap(image3);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 203) {
            if (resultCode == RESULT_OK) {
                image4 = (Bitmap) data.getExtras().get("data");
                imageUser4 = ConvertBitmapToString(image4);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image4.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img4.setImageBitmap(image4);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 204) {
            if (resultCode == RESULT_OK) {
                image5 = (Bitmap) data.getExtras().get("data");
                imageUser5 = ConvertBitmapToString(image5);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image5.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img5.setImageBitmap(image5);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 205) {
            if (resultCode == RESULT_OK) {
                image6 = (Bitmap) data.getExtras().get("data");
                imageUser6 = ConvertBitmapToString(image6);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image6.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img6.setImageBitmap(image6);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 206) {
            if (resultCode == RESULT_OK) {
                image7 = (Bitmap) data.getExtras().get("data");
                imageUser7 = ConvertBitmapToString(image7);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image7.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img7.setImageBitmap(image7);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }


    }


//    private void uploadImage(){
//        class UploadImage extends AsyncTask<Bitmap,Void,String>{
//
//            ProgressDialog loading;
//            RequestHandler rh = new RequestHandler();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                loading = new ProgressDialog(UploadImageResident.this);
//                loading.setCancelable(false);
//                loading.setTitle("Uploading...");
//                loading.setMessage("Please Wait");
//                loading.show();
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//                loading.dismiss();
//                try {
////
//                if (s != null) {
//                    JSONObject jsonResponse = new JSONObject(s);
//                    String status = jsonResponse.getString("Response");
//                    if (status.equals("Success")) {
//
//                        Intent nextPersonalDetails = new Intent(UploadImageResident.this, PersonalDetails.class);
//                        startActivity(nextPersonalDetails);
//                        finish();
//                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
//
//                    } else {
//
//                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            }
//
//            @Override
//            protected String doInBackground(Bitmap... params) {
//                Bitmap bitmap1 = params[0];
//                Bitmap bitmap2 = params[1];
//                Bitmap bitmap3 = params[2];
//                Bitmap bitmap4 = params[3];
//                Bitmap bitmap5 = params[4];
//                Bitmap bitmap6 = params[5];
//                Bitmap bitmap7 = params[6];
//                String uploadImage1 = ConvertBitmapToString(bitmap1);
//                String uploadImage2 = ConvertBitmapToString(bitmap2);
//                String uploadImage3 = ConvertBitmapToString(bitmap3);
//                String uploadImage4 = ConvertBitmapToString(bitmap4);
//                String uploadImage5 = ConvertBitmapToString(bitmap5);
//                String uploadImage6 = ConvertBitmapToString(bitmap6);
//                String uploadImage7 = ConvertBitmapToString(bitmap7);
//
//                HashMap<String,String> data = new HashMap<>();
//                data.put("id", id);
//                data.put("username",username);
//                data.put("img1", uploadImage1);
//                data.put("img2", uploadImage2);
//                data.put("img3", uploadImage3);
//                data.put("img4", uploadImage4);
//                data.put("img5", uploadImage5);
//                data.put("img6", uploadImage6);
//                data.put("img7", uploadImage7);
////                String result = rh.sendPostRequest(UPLOAD_URL_LOCAL,data);
//                String result = rh.sendPostRequest(UPLOAD_URL,data);
//
//                return result;
//            }
//        }
//        UploadImage ui = new UploadImage();
//        ui.execute(image1,image2,image3, image4, image5, image6, image7);
////    }


    class UploadImageFiResident extends AsyncTask<String, Void, String> {

        private String Content;
        private String Error = null;
        String data = "";
        private BufferedReader reader;
        ProgressDialog pDialog;
        HashMap<String,String> keyAndImage;


        protected void onPreExecute() {
            pDialog = new ProgressDialog(UploadImageResident.this);
            pDialog.setCancelable(false);
            pDialog.setTitle("Uploading...");
            pDialog.setMessage("Please Wait");
            pDialog.show();
            keyAndImage = new HashMap<>();

        }

        protected String doInBackground(String... urls) {

//            HttpURLConnection connection = null;
            try {
                URI uri = new URI(urls[0]);
                String urlString = uri.toString();
                URL url = new URL(urlString);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                con.setRequestMethod("POST");
                con.setUseCaches(false);
                con.setDoInput(true);
                con.setDoOutput(true);

                con.setConnectTimeout(15000);
                con.setReadTimeout(20000);
//                con.setRequestProperty("Content-Length", "" + data.getBytes().length);
                con.setRequestProperty("Connection", "Keep-Alive");
                con.setRequestProperty("id", id);
                con.setRequestProperty("username", username);
//                con.setRequestProperty("img1", imageUser1);
//                con.setRequestProperty("img2", imageUser2);
//                con.setRequestProperty("img3", imageUser3);
//                con.setRequestProperty("img4", imageUser4);
//                con.setRequestProperty("img5", imageUser5);
//                con.setRequestProperty("img6", imageUser6);
//                con.setRequestProperty("img7", imageUser7);
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));


                try {
                    keyAndImage.put("id",id);
                    keyAndImage.put("username",username);
                    keyAndImage.put("img1",imageUser1);
                    keyAndImage.put("img2","");
                    keyAndImage.put("img3","");
                    keyAndImage.put("img4","");
                    keyAndImage.put("img5","");
                    keyAndImage.put("img6","");
                    keyAndImage.put("img7","");

//                    data = getPostDataString(keyAndImage);

//                    data += "&img1=data:image/png;base64," + URLEncoder.encode(imageUser1, "UTF-8")
//                            +"&" + URLEncoder.encode("img2", "UTF-8") + "=" + "data:image/png;base64," + imageUser2
//                            +"&" + URLEncoder.encode("img3", "UTF-8") + "=" + "data:image/png;base64," + imageUser3
//                            +"&" + URLEncoder.encode("img4", "UTF-8") + "=" + "data:image/png;base64," + imageUser4
//                            +"&" + URLEncoder.encode("img5", "UTF-8") + "=" + "data:image/png;base64," + imageUser5
//                            +"&" + URLEncoder.encode("img6", "UTF-8") + "=" + "data:image/png;base64," + imageUser6
//                            +"&" + URLEncoder.encode("img7", "UTF-8") + "=" + "data:image/png;base64," + imageUser7
//                            +"&" + URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+
//                            "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");



//                    data += "&img1=data:image/png;base64," + URLEncoder.encode(imageUser1, "UTF-8")
//                            +"&img2=data:image/png;base64," + URLEncoder.encode(imageUser2, "UTF-8")
//                            +"&img3=data:image/png;base64," + URLEncoder.encode(imageUser3, "UTF-8")
//                            +"&img4=data:image/png;base64," + URLEncoder.encode(imageUser4, "UTF-8")
//                            +"&img5=data:image/png;base64," + URLEncoder.encode(imageUser5, "UTF-8")
//                            +"&img6=data:image/png;base64," + URLEncoder.encode(imageUser6, "UTF-8")
//                            +"&img7=data:image/png;base64," + URLEncoder.encode(imageUser7, "UTF-8")
//                            +"&" + URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+
//                            "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");

                    data += "&img1=data:image/png;base64," + URLEncoder.encode(imageUser1, "UTF-8");
                    if(imageUser2 != null && imageUser2.length() != 0) {
                        data += "&img2=data:image/png;base64," + URLEncoder.encode(imageUser2, "UTF-8");
                    }
                    if(imageUser3 != null && imageUser3.length() != 0) {
                        data += "&img3=data:image/png;base64," + URLEncoder.encode(imageUser3, "UTF-8");
                    }
                    if(imageUser4 != null && imageUser4.length() != 0) {
                        data += "&img4=data:image/png;base64," + URLEncoder.encode(imageUser4, "UTF-8");
                    }
                    if(imageUser5 != null && imageUser5.length() != 0) {
                        data += "&img5=data:image/png;base64," + URLEncoder.encode(imageUser5, "UTF-8");
                    }
                    if(imageUser6 != null && imageUser6.length() != 0) {
                        data += "&img6=data:image/png;base64," + URLEncoder.encode(imageUser6, "UTF-8");
                    }
                    if(imageUser7 != null && imageUser7.length() != 0) {
                        data += "&img7=data:image/png;base64," + URLEncoder.encode(imageUser7, "UTF-8");
                    }
                    data += "&" + URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+
                            "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                //make request
                writer.write(data);
                writer.flush();
                writer.close();
                int code = con.getResponseCode();
                InputStream is = null;

                if (code >= 200 && code < 400) {
                    // Create an InputStream in order to extract the response object
                    is = con.getInputStream();
                }
                else {
                    is = con.getErrorStream();
                }
                reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                exception = ""+code;

                Content = sb.toString();
                return Content;
            } catch (Exception ex) {
                exception = ex.getMessage().toString();
                Error = ex.getMessage();
            }
            return null;

        }


        protected void onPostExecute(String unused) {
            // NOTE: You can call UI Element here.

            pDialog.dismiss();
//            Toast.makeText(getApplicationContext(), exception, Toast.LENGTH_SHORT).show();

            try {

                if (Content != null) {
                    JSONObject jsonResponse = new JSONObject(Content);
                    String status = jsonResponse.getString("Response");
                    if (status.equals("Success")) {

                        Intent nextPersonalDetails = new Intent(UploadImageResident.this, PersonalDetails.class);
                        startActivity(nextPersonalDetails);
                        finish();
                        Toast.makeText(getApplicationContext(), "Uploaded Successfully.", Toast.LENGTH_SHORT).show();

                    } else {

                        Toast.makeText(getApplicationContext(), unused, Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public static String ConvertBitmapToString(Bitmap bitmap){
        String encodedImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        encodedImage= Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

        return encodedImage;
    }

//    public static String ConvertBitmapToString(Bitmap bitmap){
//        String encodedImage = "";
//
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        if(bitmap != null) {
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//
//                encodedImage= Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
//
//        }else
//            encodedImage = "";
//
////        return encodedImage;
//        return "data:image/png;base64,"+encodedImage;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
