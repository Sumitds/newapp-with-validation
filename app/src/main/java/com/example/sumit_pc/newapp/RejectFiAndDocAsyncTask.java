package com.example.sumit_pc.newapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.sumit_pc.newapp.DocDataActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by sumit-pc on 23/11/17.
 */

public class RejectFiAndDocAsyncTask extends AsyncTask<String,Void,String> {
    String json_getUrl = "";
    String username = "";
    String type = "";
    String caseId = "";
    String rejectionReason = "";
    String JSON_STRING = "";
    Context activityContext = null;
    RejectionCallBack status;
    ProgressDialog progressDialog;

    RejectFiAndDocAsyncTask(Context activityContext, RejectionCallBack rejectionCallBack){
        this.activityContext = activityContext;
        status = rejectionCallBack;
    }

    @Override
    protected void onPreExecute() {
        json_getUrl = "http://www.nscs.in/api/reject";
        progressDialog = new ProgressDialog(activityContext);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {

        try {
            this.username = params[0];
            this.type = params[1];
            this.caseId = params[2];
            this.rejectionReason = params[3];

            URL loginURL = new URL(json_getUrl);
            Log.d("loginURL","loginURL reached");
            HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();
            Log.d("httpconn","httpconn reached");

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setAllowUserInteraction(false);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpURLConnection.setRequestProperty("username",this.username);
            httpURLConnection.setRequestProperty("type",this.type);
            httpURLConnection.setRequestProperty("id",caseId);
            httpURLConnection.setRequestProperty("rr",this.rejectionReason);

            OutputStream OS = httpURLConnection.getOutputStream();
            BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = "";
            try {
                data =  URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(this.username,"UTF-8")+"&"+
                        URLEncoder.encode("type","UTF-8") + "=" + URLEncoder.encode(this.type,"UTF-8")+"&"+
                        URLEncoder.encode("id","UTF-8") + "=" + URLEncoder.encode(this.caseId,"UTF-8")+"&"+
                        URLEncoder.encode("rr","UTF-8") + "=" + URLEncoder.encode   (this.rejectionReason,"UTF-8");

            }catch (NullPointerException npe){
                npe.printStackTrace();
            }

            BW.write(data);
            BW.flush();
            BW.close();
            OS.close();

            InputStream IS = httpURLConnection.getInputStream();

            Log.d("IS","IS reached");
            BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
            Log.d("BR","BR reached");
            StringBuilder sb = new StringBuilder();

            while ((JSON_STRING = BR.readLine())!=null){
                sb.append(JSON_STRING+"\n");
            }

            String jsn = sb.toString().trim();
            Log.d("jsn",jsn);
            BR.close();
            IS.close();

            httpURLConnection.disconnect();

            return  jsn;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();
        String responseStatus = "";
        try {
            JSONObject jsonObject = new JSONObject(result);
            responseStatus = jsonObject.getString("Response");
            if(responseStatus.equals("Success")){
                status.onSuccess("success");

            }else{
                Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
            }
    } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
