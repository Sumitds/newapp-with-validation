package com.example.sumit_pc.newapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.residenceverification.UploadImageResident;

import java.util.List;

public class FiDataAdaptor extends RecyclerView.Adapter<FiDataAdaptor.MyViewHolder> {


    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE_RESIDENT = 200;
    private Context context;
    private Context mContext;
    private List<FiDataParser.DataBean> fiDataList;
    LayoutInflater inflater;
    String user = "";
    String id = "";
    String caseIdFi = "";
    String rejectionReason = "", fiDataUserShared="", userCaseId="";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView caseId, clientName, productName, customerName, verificationType, officeOrHomeAddr;
        public Button submit, reject;
        public MyViewHolder(View view) {
            super(view);
            caseId = (TextView) view.findViewById(R.id.case_id_fi);
            clientName = (TextView) view.findViewById(R.id.client_name_fi);
            productName = (TextView) view.findViewById(R.id.product_type_fi);
            customerName = (TextView) view.findViewById(R.id.customer_name_fi);
            verificationType = (TextView) view.findViewById(R.id.verification_type_fi);
            officeOrHomeAddr = (TextView) view.findViewById(R.id.office_or_home_addr_fi);
            reject = (Button) view.findViewById(R.id.reject_fi);
            submit = (Button) view.findViewById(R.id.submit_fi);


        }

    }


    public FiDataAdaptor(List<FiDataParser.DataBean> fiDataList, String username, Context context) {
        this.fiDataList = fiDataList;
        user = username;
        this.context = context;
    }

    @Override
    public FiDataAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_fi_data, parent, false);
        mContext = parent.getContext();
        sharedPreferences = mContext.getSharedPreferences("FiDataUser",Context.MODE_PRIVATE);

        editor = sharedPreferences.edit();
        editor.clear();

        return new FiDataAdaptor.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FiDataAdaptor.MyViewHolder holder, final int position) {
        final String rejectionType = "fi";
        final FiDataParser.DataBean fiDataParser = fiDataList.get(position);
        holder.caseId.setText("Case Id : "+fiDataParser.getCase_id());
        holder.clientName.setText("Client Name : "+fiDataParser.getClient_name());
        holder.productName.setText("Product Name : "+fiDataParser.getProduct_name());
        holder.customerName.setText("Customer Name : "+fiDataParser.getCustomer_name());
        holder.verificationType.setText("Verification Type : "+fiDataParser.getVerification_type());
        holder.officeOrHomeAddr.setText("Office / Home Address : " + fiDataParser.getOffice_or_home_address());

        holder.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Activity origin = (Activity) context;
                caseIdFi = fiDataParser.getCase_id();
            FiAndDocSeenAsyncTask fiAndDocSeenAsyncTask = new FiAndDocSeenAsyncTask(mContext, new SeenCallBack() {
                @Override
                public void onSuccess(String status) {
                if(status.equals("success"+caseIdFi)){
                    id = ""+caseIdFi;
                    if(fiDataList.get(position).getVerification_type().equals("Office")) {

                        // below code to be moved to last activity to capture images
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                        } else {

                            id = ""+caseIdFi;
                            Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                            editor.putString("username",user);
                            editor.putString("caseId",id);
                            editor.commit();
                            captureIntent.putExtra("username",user);
                            captureIntent.putExtra("caseId",caseIdFi);
                            ((FiDataActivity) mContext).startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                            // permission has been granted, continue as usual

                        }
                    }else if(fiDataList.get(position).getVerification_type().equals("Residential") || fiDataList.get(position).getVerification_type().equals("Property")){
                            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED) {
                                // Check Permissions Now
                                ActivityCompat.requestPermissions((FiDataActivity) mContext,
                                        new String[]{Manifest.permission.CAMERA},
                                        UploadImageResident.CAMERA_CAPTURE_IMAGE_REQUEST_CODE_RESIDENT);
                            }else{
                                Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                id = ""+caseIdFi;
                                editor.putString("username",user);
                                editor.putString("caseId",id);
                                editor.commit();
                                captureIntent.putExtra("id",caseIdFi);
                                captureIntent.putExtra("username",user);
                                ((FiDataActivity) mContext).startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE_RESIDENT);
                            }
                    }else{
                        Toast.makeText(mContext,"Unknown Verification type", Toast.LENGTH_LONG).show();
                    }
                }
                }
                });
                fiAndDocSeenAsyncTask.execute(user,rejectionType,caseIdFi);

            }

        });

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                caseIdFi = fiDataParser.getCase_id();
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
                dialogBuilder.setTitle("Reason For Rejection");
                dialogBuilder.setCancelable(false);
                inflater = LayoutInflater.from(mContext);
                final View dialogView = inflater.inflate(R.layout.reject_dialog, null);


            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String dataCorrectionCode = "1";
                    String notRelatedCode = "2";
                   RadioGroup radioGroup =  dialogView.findViewById(R.id.reject_dialog_radio_button);
                   RadioButton notRelated =  dialogView.findViewById(R.id.not_related);
                   RadioButton dataCurropted =  dialogView.findViewById(R.id.data_corrupted);
                if(dataCurropted.isChecked() || notRelated.isChecked()) {

                    switch (radioGroup.getCheckedRadioButtonId()) {
                        case R.id.not_related:
                            if (notRelated.isChecked()) {
                                rejectionReason = notRelatedCode;
                                RejectFiAndDocAsyncTask rejectFiAndDocAsyncTask =
                                        new RejectFiAndDocAsyncTask(mContext, new RejectionCallBack() {
                                    @Override
                                    public void onSuccess(String status) {
                                        if(status.equals("success")){
                                        FiDataParser.DataBean d = fiDataList.get(position);
                                        fiDataList.remove(position);
                                        notifyItemRemoved(position);
                                        notifyItemRangeChanged(position, fiDataList.size());
                                            Toast.makeText(mContext,"Case Rejected Successfully!!",Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                });
                                rejectFiAndDocAsyncTask.execute(user,rejectionType,caseIdFi,rejectionReason);
                            break;
                            }
                        case R.id.data_corrupted:
                            if (dataCurropted.isChecked()) {
                                rejectionReason = dataCorrectionCode;
                                RejectFiAndDocAsyncTask rejectFiAndDocAsyncTask =
                                    new RejectFiAndDocAsyncTask(mContext, new RejectionCallBack() {
                                        @Override
                                        public void onSuccess(String status) {
                                        if (status.equals("success")) {
                                            FiDataParser.DataBean d = fiDataList.get(position);
                                            fiDataList.remove(position);
                                            notifyItemRemoved(position);
                                            notifyItemRangeChanged(position, fiDataList.size());
                                            Toast.makeText(mContext,"Case Rejected Successfully!!",Toast.LENGTH_SHORT).show();
                                        }
                                        }
                                    });
                                rejectFiAndDocAsyncTask.execute(user,rejectionType,caseIdFi,rejectionReason);
                        break;
                            }
                    }
                }else{

                    Toast.makeText(mContext, "Please select an option for rejecting", Toast.LENGTH_LONG).show();

                }
                }
                });

                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                dialogBuilder.setView(dialogView);
                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                alertDialog.show();
            }

        });
    }

    @Override
    public int getItemCount() {
        return fiDataList.size();
    }

}