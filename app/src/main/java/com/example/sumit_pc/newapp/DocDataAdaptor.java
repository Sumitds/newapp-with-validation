package com.example.sumit_pc.newapp;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.sumit_pc.newapp.officeverification.UploadPhotos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumit-pc on 18/10/17.
 */

public class DocDataAdaptor extends RecyclerView.Adapter<DocDataAdaptor.MyViewHolder> {

    private Context mContext;
    private Context context;
    private List<DocDataParser.DataBean> docDataList;
    LayoutInflater inflater;
    String user = "";
    private static final int REQUEST_ID_READ_PERMISSION = 100;
    private static final int REQUEST_ID_WRITE_PERMISSION = 200;
    String caseIdDoc = "";
    String rejectionReason = "";
    private static ArrayList<String> file_url;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView caseId, clientName, productName, customerName, docName, deptName, deptAddr;
        public Button submit, reject;
        Button docDownloadBtn;


        public MyViewHolder(View view) {
            super(view);
            caseId = (TextView) view.findViewById(R.id.case_id_doc);
            clientName = (TextView) view.findViewById(R.id.client_name_doc);
            productName = (TextView) view.findViewById(R.id.product_name_doc);
            customerName = (TextView) view.findViewById(R.id.customer_name_doc);
            docName = (TextView) view.findViewById(R.id.doc_name);
            deptName = (TextView) view.findViewById(R.id.dept_name_doc);
            deptAddr = (TextView) view.findViewById(R.id.dept_addr_doc);
            reject = (Button) view.findViewById(R.id.reject_doc);
            submit = (Button) view.findViewById(R.id.submit_doc);
            docDownloadBtn = (Button) view.findViewById(R.id.docDownload);

        }
    }


    public DocDataAdaptor(List<DocDataParser.DataBean> docDataList, String username, Context context) {
        this.user = username;
        this.docDataList = docDataList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_doc_data, parent, false);
        mContext = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final String rejectionType = "doc";
        final DocDataParser.DataBean docDataParser = docDataList.get(position);
        holder.caseId.setText("Case Id : "+docDataParser.getCase_id());
        holder.clientName.setText("Client Name : "+docDataParser.getClient_name());
        holder.productName.setText("Product Name : "+docDataParser.getProduct_name());
        holder.customerName.setText("Customer Name : "+docDataParser.getCustomer_name());
        holder.docName.setText("Doc Name : "+docDataParser.getDoc_name());
        holder.deptName.setText("Department Name : "+docDataParser.getDept_name());
        holder.deptAddr.setText("Department Address : "+docDataParser.getDept_address());

        holder.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Activity origin = (Activity) context;
                caseIdDoc = docDataParser.getCase_id();
                FiAndDocSeenAsyncTask fiAndDocSeenAsyncTask = new FiAndDocSeenAsyncTask(mContext, new SeenCallBack() {
                    @Override
                    public void onSuccess(String status) {
                        if(status.equals("success"+caseIdDoc)){
                            Intent docForm =  new Intent(context, DocSubmitActivity.class);
                            docForm.putExtra("username",user);
                            docForm.putExtra("caseId",caseIdDoc);
                            mContext.startActivity(docForm);
//                            origin.startActivityForResult(docForm,500);

                        }else{
                            Toast.makeText(mContext, "Verification type error", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                fiAndDocSeenAsyncTask.execute(user,rejectionType,caseIdDoc);
            }
        });

        holder.docDownloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Log.e("Permission error","You have permission");

                }
                String docPath = docDataParser.getDownloadLink();
                String url_file = "http://www.nscs.in/"+docPath;
                if(url_file.equals("null") || url_file.equals("")){
                    holder.docDownloadBtn.setActivated(false);
                    holder.docDownloadBtn.setAlpha(0.4f);
                    Toast.makeText(mContext,"Download file Unavailable for this Case Id",Toast.LENGTH_LONG).show();
                }else{
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url_file));
                    request.setTitle("File Download.");
                    request.setDescription("Downloading...");
                    request.allowScanningByMediaScanner();
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);

                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                    String fileName = URLUtil.guessFileName(url_file, null,
                            MimeTypeMap.getFileExtensionFromUrl(url_file));

                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);

                    DownloadManager downloadManager = (DownloadManager) mContext.getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
                    if(downloadManager != null){
                    downloadManager.enqueue(request);
                    Toast.makeText(mContext, "Download Complete", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            caseIdDoc = docDataParser.getCase_id();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            dialogBuilder.setTitle("Reason For Rejection");
            dialogBuilder.setCancelable(false);
            inflater = LayoutInflater.from(mContext);
            final View dialogView = inflater.inflate(R.layout.reject_dialog, null);

                dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    String dataCorrectionCode = "1";
                    String notRelatedCode = "2";
                    RadioGroup radioGroup =  dialogView.findViewById(R.id.reject_dialog_radio_button);
                    RadioButton notRelated =  dialogView.findViewById(R.id.not_related);
                    RadioButton dataCurropted =  dialogView.findViewById(R.id.data_corrupted);
                        if(dataCurropted.isChecked() || notRelated.isChecked()) {
                        switch (radioGroup.getCheckedRadioButtonId()){
                            case R.id.not_related:
                                if (notRelated.isChecked()) {
                                    rejectionReason = notRelatedCode;
                                    RejectFiAndDocAsyncTask rejectFiAndDocAsyncTask =
                                        new RejectFiAndDocAsyncTask(mContext, new RejectionCallBack() {
                                    @Override
                                    public void onSuccess(String status) {
                                        if(status.equals("success")){
                                            DocDataParser.DataBean d = docDataList.get(position);
                                            docDataList.remove(position);
                                            notifyItemRemoved(position);
                                            notifyItemRangeChanged(position, docDataList.size());
                                            Toast.makeText(mContext,"Case Rejected Successfully!!",Toast.LENGTH_SHORT).show();
                                        }
                                        }
                                    });
                                    rejectFiAndDocAsyncTask.execute(user,rejectionType,caseIdDoc,rejectionReason);
                                    break;
                                }
                            case R.id.data_corrupted:
                                if (dataCurropted.isChecked()) {
                                    rejectionReason = dataCorrectionCode;
                                    RejectFiAndDocAsyncTask rejectFiAndDocAsyncTask =
                                    new RejectFiAndDocAsyncTask(mContext, new RejectionCallBack() {
                                @Override
                                public void onSuccess(String status) {
                                    if(status.equals("success")){
                                        DocDataParser.DataBean d = docDataList.get(position);
                                        docDataList.remove(position);
                                        notifyItemRemoved(position);
                                        notifyItemRangeChanged(position, docDataList.size());
                                        Toast.makeText(mContext,"Case Rejected Successfully!!",Toast.LENGTH_SHORT).show();
                                    }
                                        }
                                    });
                                    rejectFiAndDocAsyncTask.execute(user,rejectionType,caseIdDoc,rejectionReason);
                                    break;
                                }
                        }
                        }else{

                            Toast.makeText(mContext, "Please select an option for rejecting", Toast.LENGTH_LONG).show();

                        }
                    }
                });

                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

                dialogBuilder.setView(dialogView);

                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                alertDialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return docDataList.size();
    }

}
