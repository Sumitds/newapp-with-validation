package com.example.sumit_pc.newapp.residenceverification;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.ToastMsg;
import com.example.sumit_pc.newapp.officeverification.GeneralDetailsFiSubmit;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonalDetails extends AppCompatActivity {

    EditText personContacted,dob, relationship, nameAndAddressOfCompany, yearsInCityAndCurrentResidence,
            residenceWithinCityLimit, maritalStatus,spouseDetails,qualificationApplicant,residenceProofVerified,
                permanentAddressAndPhone, numberOfFamilyMembersWithApplicant, numberOfMembersEarning, monthlyIncome, dependents,
                otherLoanObligation, existingVehicleDetails, vehicleRegistrationNumber, vehicleFreeOrFinance, existingLoanObligation;
    Calendar calendar;
    Button nextObservation;
    int year,month,day;
    Spinner residentialStatus,localityType, locality, familyStructure;
    ArrayAdapter<CharSequence> residentialStatusAdapter, localityTypeAdapter, localityAdapter, familyStructureAdapter;
    final String personalSharePreference = "PersonalDetails";
    final String uploadPhotos = "UploadPhotosResident";
    SharedPreferences sharedPreferences, sharedPreferences1;
    SharedPreferences.Editor editor ;

    ActionBar actionBar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);


        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        dob = (EditText) findViewById(R.id.date_of_birth);
        personContacted = (EditText) findViewById(R.id.person_contacted_resident);
        relationship = (EditText) findViewById(R.id.relationship_resident);
        nameAndAddressOfCompany = (EditText) findViewById(R.id.name_and_address_and_designation_company_firm);
        yearsInCityAndCurrentResidence = (EditText) findViewById(R.id.years_in_city_and_residence);
        residenceWithinCityLimit = (EditText) findViewById(R.id.residence_within_city_limits);
        permanentAddressAndPhone = (EditText) findViewById(R.id.permanent_addr_and_phone);
        numberOfFamilyMembersWithApplicant = (EditText) findViewById(R.id.no_of_family_member_with_applicant);
        numberOfMembersEarning = (EditText) findViewById(R.id.no_of_earning_members);
        monthlyIncome = (EditText) findViewById(R.id.monthly_income_and_details);
        dependents = (EditText) findViewById(R.id.dependents);
        existingLoanObligation = (EditText) findViewById(R.id.existing_loan_obligations_personal);
        existingVehicleDetails = (EditText) findViewById(R.id.existing_vehicle_details_personal);
        vehicleRegistrationNumber = (EditText) findViewById(R.id.vehicle_reg_number_personal);
        vehicleFreeOrFinance = (EditText) findViewById(R.id.free_or_finance_details_personal);
        otherLoanObligation = (EditText) findViewById(R.id.other_loan_obligations_personal);
        maritalStatus = (EditText) findViewById(R.id.marital_status_personal);
        spouseDetails = (EditText) findViewById(R.id.spouse_details);
        qualificationApplicant = (EditText) findViewById(R.id.qualification_applicant);
        residenceProofVerified = (EditText) findViewById(R.id.residence_proof_verified);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        residentialStatus = (Spinner) findViewById(R.id.residential_status);
        residentialStatusAdapter = ArrayAdapter.createFromResource(this,
                R.array.residential_status_options, android.R.layout.simple_spinner_item);
        residentialStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        residentialStatus.setAdapter(residentialStatusAdapter);

        localityType = (Spinner) findViewById(R.id.locality_type);
        localityTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.locality_type_options, android.R.layout.simple_spinner_item);
        localityTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        localityType.setAdapter(localityTypeAdapter);

        locality = (Spinner) findViewById(R.id.locality);
        localityAdapter = ArrayAdapter.createFromResource(this,
                R.array.locality_options, android.R.layout.simple_spinner_item);
        localityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locality.setAdapter(localityAdapter);

        familyStructure = (Spinner) findViewById(R.id.family_structure);
        familyStructureAdapter = ArrayAdapter.createFromResource(this,
                R.array.family_structure_options, android.R.layout.simple_spinner_item);
        familyStructureAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        familyStructure.setAdapter(familyStructureAdapter);

        nextObservation = (Button) findViewById(R.id.next_observations);
        sharedPreferences = getSharedPreferences(personalSharePreference, Context.MODE_PRIVATE);
        sharedPreferences1 = getSharedPreferences(uploadPhotos, Context.MODE_PRIVATE);
        nextObservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastMsg toastMsg = new ToastMsg(PersonalDetails.this);
                View view = PersonalDetails.this.getCurrentFocus();
                if(personContacted.getText().toString().equals("") || relationship.getText().toString().equals("")
                        || nameAndAddressOfCompany.getText().toString().equals("") || dob.getText().toString().equals("")){

                    toastMsg.showToast("Fields with (*) mark are mandatory.");
                }else{
                    if(toastMsg.isContainSpecialCharOnly(personContacted.getText().toString())
                       && toastMsg.isContainSpecialCharOnly(relationship.getText().toString())
                       && toastMsg.isContainSpecialCharOnly(nameAndAddressOfCompany.getText().toString())
                       && toastMsg.isContainSpecialCharOnly(maritalStatus.getText().toString())
                       && toastMsg.isContainSpecialChar(vehicleRegistrationNumber.getText().toString())
                       && toastMsg.isContainSpecialChar(residenceWithinCityLimit.getText().toString())
                       && toastMsg.isContainSpecialChar(spouseDetails.getText().toString())
                       && toastMsg.isContainSpecialChar(residenceProofVerified.getText().toString())
                       && toastMsg.isContainSpecialChar(permanentAddressAndPhone.getText().toString())
                       && toastMsg.isContainSpecialChar(yearsInCityAndCurrentResidence.getText().toString())
                       && toastMsg.isContainSpecialChar(qualificationApplicant.getText().toString())
                       && toastMsg.isContainSpecialChar(monthlyIncome.getText().toString())
                       && toastMsg.isContainSpecialChar(existingLoanObligation.getText().toString())
                       && toastMsg.isContainSpecialChar(existingVehicleDetails.getText().toString())
                       && toastMsg.isContainSpecialChar(otherLoanObligation.getText().toString())
                       && toastMsg.isContainSpecialChar(vehicleFreeOrFinance.getText().toString())){
                    storePersonalDetails();
                    Intent nextObservation = new Intent(PersonalDetails.this, Observations.class);
                    startActivity(nextObservation);
                    }else {
                        toastMsg.showToast("No special character or emoticon allowed.");

                    }
                }


            }
        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                showDialog(999);

            }
        });
    }

    void storePersonalDetails(){

//        String id = getIntent().getStringExtra("caseId");
//        String username = getIntent().getStringExtra("username");
        String id = sharedPreferences1.getString("caseId","");
        String username = sharedPreferences1.getString("username","");

        editor = sharedPreferences.edit();
        editor.clear();
        editor.putString("id", id);
        editor.putString("username",username);
        editor.putString("dob",dob.getText().toString());
        editor.putString("personContacted",personContacted.getText().toString());
        editor.putString("relationship",relationship.getText().toString());
        editor.putString("nameAndAddressOfCompany",nameAndAddressOfCompany.getText().toString());
        editor.putString("yearsInCityAndCurrentResidence",yearsInCityAndCurrentResidence.getText().toString());
        editor.putString("residenceWithinCityLimit",residenceWithinCityLimit.getText().toString());
        editor.putString("permanentAddressAndPhone",permanentAddressAndPhone.getText().toString());
        editor.putString("numberOfFamilyMembersWithApplicant",numberOfFamilyMembersWithApplicant.getText().toString());
        editor.putString("numberOfMembersEarning",numberOfMembersEarning.getText().toString());
        editor.putString("monthlyIncome",monthlyIncome.getText().toString());
        editor.putString("dependents",dependents.getText().toString());
        editor.putString("existingVehicleDetails",existingVehicleDetails.getText().toString());
        editor.putString("residentialStatus",residentialStatus.getSelectedItem().toString());
        editor.putString("localityType",localityType.getSelectedItem().toString());
        editor.putString("locality",locality.getSelectedItem().toString());
        editor.putString("familyStructure",familyStructure.getSelectedItem().toString());
        editor.putString("vehicleRegistrationNumber",vehicleRegistrationNumber.getText().toString());
        editor.putString("vehicleFreeOrFinance",vehicleFreeOrFinance.getText().toString());
        editor.putString("existingLoanObligation",existingLoanObligation.getText().toString());
        editor.putString("otherLoanObligation",otherLoanObligation.getText().toString());
        editor.putString("maritalStatus",maritalStatus.getText().toString());
        editor.putString("spouseDetails",spouseDetails.getText().toString());
        editor.putString("qualificationApplicant",qualificationApplicant.getText().toString());
        editor.putString("residenceProofVerified",residenceProofVerified.getText().toString());
        editor.commit();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,myDateListener,year,month,day);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return datePickerDialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            showDate(arg1, arg2+1, arg3);
        }
    };
    private void showDate(int year, int month, int day) {
        dob.setText(new StringBuilder().append(day).append("/").append(month).append("/").append(year));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //working code to save data even after going next activity
//    @Override
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        super.onSaveInstanceState(savedInstanceState);
//        // Save UI state changes to the savedInstanceState.
//        // This bundle will be passed to onCreate if the process is
//        // killed and restarted.
//        savedInstanceState.putString("personContacted",personContacted.getText().toString());
//        // etc.
//    }
//
//    @Override
//    public void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        // Restore UI state from the savedInstanceState.
//        // This bundle has also been passed to onCreate.
//        String myString = savedInstanceState.getString("personContacted");
//        personContacted.setText(myString);
//    }
}
