package com.example.sumit_pc.newapp.residenceverification;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sumit_pc.newapp.CheckNetwork;
import com.example.sumit_pc.newapp.FiDataActivity;
import com.example.sumit_pc.newapp.FiDataFetchAsyncTask;
import com.example.sumit_pc.newapp.FiResidentialSubmitCallBack;
import com.example.sumit_pc.newapp.ProfileDashboardActivity;
import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.officeverification.OtherDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


public class OtherDetailsResidence extends AppCompatActivity {

    String otherResidentialDetailSharedpreferences = "OtherDetailsResidence";
    String personalSharePreference = "PersonalDetails";
    String observationsSharePreference = "Observations";
    String neighbourFeedBackSharedPreferences = "NeighbourFeedback";
//    SharedPreferences.Editor editorOtherDetailsResidence, editorObservations, editorNeighbourFeedback, editorPersonalDetails;
    SharedPreferences personalDetailsShared, ObservationShared, neighbourShared, otherDetailsShared;

    EditText anyConcern, remarkByVerifier;
    Button fiDataSubmit;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_details_residence);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        anyConcern = (EditText) findViewById(R.id.any_concern_residence);
        remarkByVerifier = (EditText) findViewById(R.id.verifiers_remark_residence);
        fiDataSubmit = (Button) findViewById(R.id.submit_residence_details);;

        fiDataSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckNetwork.isInternetAvailable(OtherDetailsResidence.this)) {
                    SubmitFiResidentAsyncTask submitFiResidentAsyncTask = new SubmitFiResidentAsyncTask(new FiResidentialSubmitCallBack() {
                        @Override
                        public void onSuccess(String success) {
                            if(success.equals("success")){
                                Toast.makeText(OtherDetailsResidence.this, "Loading...", Toast.LENGTH_LONG).show();
                                String usr = personalDetailsShared.getString("username","");
                                FiDataFetchAsyncTaskAfterSubmit fiDataFetchAsyncTask = new FiDataFetchAsyncTaskAfterSubmit(OtherDetailsResidence.this, usr);
                                fiDataFetchAsyncTask.execute(usr);
//                                Intent i = new Intent(OtherDetailsResidence.this,ProfileDashboardActivity.class);
//                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(i);

//                                String user = personalDetailsShared.getString("username","");
//                                FiDataFetchAsyncTask fiDataFetchAsyncTask = new FiDataFetchAsyncTask(OtherDetailsResidence.this,user);
//                                fiDataFetchAsyncTask.execute(user);
//                                finish();
                            }
                        }
                    });
                    submitFiResidentAsyncTask.execute(anyConcern.getText().toString(),remarkByVerifier.getText().toString());

                }else{
                    Snackbar snackbar = Snackbar
                            .make(findViewById(R.id.coordinatorLayout), "No Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();}
                }
        });


    }


    public class SubmitFiResidentAsyncTask extends AsyncTask<String, Void, String> {
        String url = "http://www.nscs.in/api/r_submit", JSON_STRING="";
        FiResidentialSubmitCallBack fiResidentialSubmitCallBack;
        ProgressDialog progressDialog;

        SubmitFiResidentAsyncTask(FiResidentialSubmitCallBack fiResidentialSubmitCallBack){
            this.fiResidentialSubmitCallBack = fiResidentialSubmitCallBack;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            personalDetailsShared = getSharedPreferences(personalSharePreference, Context.MODE_PRIVATE);
            ObservationShared = getSharedPreferences(observationsSharePreference, Context.MODE_PRIVATE);
            neighbourShared = getSharedPreferences(neighbourFeedBackSharedPreferences, Context.MODE_PRIVATE);
            otherDetailsShared = getSharedPreferences(otherResidentialDetailSharedpreferences, Context.MODE_PRIVATE);

            progressDialog = new ProgressDialog(OtherDetailsResidence.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Submitting...");
            progressDialog.setMessage("Please wait");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String od1 = params[0];
                String od2 = params[1];
                String id =  personalDetailsShared.getString("id","");
                String user = personalDetailsShared.getString("username","");


                URL loginURL = new URL(url);
                Log.d("loginURL","loginURL reached");
                HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();
                Log.d("httpconn","httpconn reached");

                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
//                httpURLConnection.setReadTimeout(15000);
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setAllowUserInteraction(false);


                //personalResidentDetails
//                httpURLConnection.setRequestProperty("id", "21");
                httpURLConnection.setRequestProperty("id",id);
//                httpURLConnection.setRequestProperty("username","fe3");
                httpURLConnection.setRequestProperty("username",user);
                httpURLConnection.setRequestProperty("pd1",personalDetailsShared.getString("personContacted",""));
                httpURLConnection.setRequestProperty("pd2",personalDetailsShared.getString("relationship",""));
                httpURLConnection.setRequestProperty("pd3",personalDetailsShared.getString("nameAndAddressOfCompany",""));
                httpURLConnection.setRequestProperty("pd4",personalDetailsShared.getString("dob",""));
                httpURLConnection.setRequestProperty("pd5",personalDetailsShared.getString("residentialStatus",""));
                httpURLConnection.setRequestProperty("pd6",personalDetailsShared.getString("yearsInCityAndCurrentResidence",""));
                httpURLConnection.setRequestProperty("pd7",personalDetailsShared.getString("residenceWithinCityLimit",""));
                httpURLConnection.setRequestProperty("pd8",personalDetailsShared.getString("permanentAddressAndPhone",""));
                httpURLConnection.setRequestProperty("pd9",personalDetailsShared.getString("localityType",""));
                httpURLConnection.setRequestProperty("pd10",personalDetailsShared.getString("locality",""));
                httpURLConnection.setRequestProperty("pd11",personalDetailsShared.getString("maritalStatus",""));
                httpURLConnection.setRequestProperty("pd12",personalDetailsShared.getString("spouseDetails",""));
                httpURLConnection.setRequestProperty("pd13",personalDetailsShared.getString("qualificationApplicant",""));
                httpURLConnection.setRequestProperty("pd14",personalDetailsShared.getString("residenceProofVerified",""));
                httpURLConnection.setRequestProperty("pd15",personalDetailsShared.getString("familyStructure",""));
                httpURLConnection.setRequestProperty("pd16",personalDetailsShared.getString("numberOfFamilyMembersWithApplicant",""));
                httpURLConnection.setRequestProperty("pd17",personalDetailsShared.getString("numberOfMembersEarning",""));
                httpURLConnection.setRequestProperty("pd18",personalDetailsShared.getString("monthlyIncome",""));
                httpURLConnection.setRequestProperty("pd19",personalDetailsShared.getString("dependents",""));
                httpURLConnection.setRequestProperty("pd20",personalDetailsShared.getString("existingLoanObligation",""));
                httpURLConnection.setRequestProperty("pd21",personalDetailsShared.getString("existingVehicleDetails",""));
                httpURLConnection.setRequestProperty("pd22",personalDetailsShared.getString("vehicleRegistrationNumber",""));
                httpURLConnection.setRequestProperty("pd23",personalDetailsShared.getString("vehicleFreeOrFinance",""));
                httpURLConnection.setRequestProperty("pd24",personalDetailsShared.getString("otherLoanObligation",""));
                //Observationdetails
                httpURLConnection.setRequestProperty("o1",ObservationShared.getString("typeOfResidence",""));
                httpURLConnection.setRequestProperty("o2",ObservationShared.getString("locality",""));
                httpURLConnection.setRequestProperty("o3",ObservationShared.getString("areaInSqft",""));
                httpURLConnection.setRequestProperty("o4",ObservationShared.getString("societyBoardSeen",""));
                httpURLConnection.setRequestProperty("o5",ObservationShared.getString("accessibilityAndDistance",""));
                httpURLConnection.setRequestProperty("o6",ObservationShared.getString("cityWithinLimit",""));
                httpURLConnection.setRequestProperty("o7",ObservationShared.getString("constructionOfHouse",""));
                httpURLConnection.setRequestProperty("o8",ObservationShared.getString("residenceEnteranceMotorable",""));
                httpURLConnection.setRequestProperty("o9",ObservationShared.getString("prominentLandmark",""));
                httpURLConnection.setRequestProperty("o10",ObservationShared.getString("namePlate",""));
                httpURLConnection.setRequestProperty("o11",ObservationShared.getString("furnishingDetails",""));
                httpURLConnection.setRequestProperty("o12",ObservationShared.getString("carPark",""));
                httpURLConnection.setRequestProperty("o13",ObservationShared.getString("residenceExteriorDetails",""));
                httpURLConnection.setRequestProperty("o14",ObservationShared.getString("typeOfFlooring",""));
                httpURLConnection.setRequestProperty("o15",ObservationShared.getString("typeOfRoofing",""));
                httpURLConnection.setRequestProperty("o16",ObservationShared.getString("numberOfRoomAndApproxSize",""));
                httpURLConnection.setRequestProperty("o17",ObservationShared.getString("assetSeen",""));
                httpURLConnection.setRequestProperty("o18",ObservationShared.getString("standardLiving",""));
                httpURLConnection.setRequestProperty("o19",ObservationShared.getString("entryPermitted",""));
                httpURLConnection.setRequestProperty("o20",ObservationShared.getString("politicalPartyAffiliation",""));
                httpURLConnection.setRequestProperty("o21",ObservationShared.getString("routeMap",""));
                //NeighbourFeedback
                httpURLConnection.setRequestProperty("n1a",neighbourShared.getString("nameAndAddress1",""));
                httpURLConnection.setRequestProperty("n1b",neighbourShared.getString("applicantStayAtGivenAddress1",""));
                httpURLConnection.setRequestProperty("n1c",neighbourShared.getString("numberOfYearsBeenStaying1",""));
                httpURLConnection.setRequestProperty("n1d",neighbourShared.getString("ownershipStatus1",""));
                httpURLConnection.setRequestProperty("n2a",neighbourShared.getString("nameAndAddress2",""));
                httpURLConnection.setRequestProperty("n2b",neighbourShared.getString("applicantStayAtGivenAddress2",""));
                httpURLConnection.setRequestProperty("n2c",neighbourShared.getString("numberOfYearsBeenStaying2",""));
                httpURLConnection.setRequestProperty("n2d",neighbourShared.getString("ownershipStatus2",""));
                //otherResidentDetails
                httpURLConnection.setRequestProperty("od1",od1);
                httpURLConnection.setRequestProperty("od2",od2);
                //extra variables
                httpURLConnection.setRequestProperty("SP","");
                httpURLConnection.setRequestProperty("SEP","");




                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                            //OtherDetails
                String data =
                        //personalDetails
                        URLEncoder.encode("id","UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+"&"+
//                        URLEncoder.encode("id","UTF-8") + "=" + URLEncoder.encode("2","UTF-8")+"&"+
                        URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(user,"UTF-8")+"&"+
//                        URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode("fe3","UTF-8")+"&"+
                        URLEncoder.encode("pd1","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("personContacted",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd2","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("relationship",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd3","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("nameAndAddressOfCompany",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd4","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("dob",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd5","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("residentialStatus",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd6","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("yearsInCityAndCurrentResidence",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd7","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("residenceWithinCityLimit",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd8","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("permanentAddressAndPhone",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd9","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("localityType",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd10","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("locality",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd11","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("maritalStatus",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd12","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("spouseDetails",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd13","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("qualificationApplicant",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd14","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("residenceProofVerified",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd15","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("familyStructure",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd16","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("numberOfFamilyMembersWithApplicant",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd17","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("numberOfMembersEarning",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd18","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("monthlyIncome",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd19","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("dependents",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd20","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("existingLoanObligation",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd21","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("existingVehicleDetails",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd22","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("vehicleRegistrationNumber",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd23","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("vehicleFreeOrFinance",""),"UTF-8")+"&"+
                        URLEncoder.encode("pd24","UTF-8") + "=" + URLEncoder.encode(personalDetailsShared.getString("otherLoanObligation",""),"UTF-8")+"&"+
                        //oBSERVATIONS
                        URLEncoder.encode("o1","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("typeOfResidence",""),"UTF-8")+"&"+
                        URLEncoder.encode("o2","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("locality",""),"UTF-8")+"&"+
                        URLEncoder.encode("o3","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("areaInSqft",""),"UTF-8")+"&"+
                        URLEncoder.encode("o4","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("societyBoardSeen",""),"UTF-8")+"&"+
                        URLEncoder.encode("o5","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("accessibilityAndDistance",""),"UTF-8")+"&"+
                        URLEncoder.encode("o6","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("cityWithinLimit",""),"UTF-8")+"&"+
                        URLEncoder.encode("o7","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("constructionOfHouse",""),"UTF-8")+"&"+
                        URLEncoder.encode("o8","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("residenceEnteranceMotorable",""),"UTF-8")+"&"+
                        URLEncoder.encode("o9","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("prominentLandmark",""),"UTF-8")+"&"+
                        URLEncoder.encode("o10","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("namePlate",""),"UTF-8")+"&"+
                        URLEncoder.encode("011","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("furnishingDetails",""),"UTF-8")+"&"+
                        URLEncoder.encode("o12","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("carPark",""),"UTF-8")+"&"+
                        URLEncoder.encode("o13","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("residenceExteriorDetails",""),"UTF-8")+"&"+
                        URLEncoder.encode("o14","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("typeOfFlooring",""),"UTF-8")+"&"+
                        URLEncoder.encode("o15","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("typeOfRoofing",""),"UTF-8")+"&"+
                        URLEncoder.encode("o16","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("numberOfRoomAndApproxSize",""),"UTF-8")+"&"+
                        URLEncoder.encode("o17","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("assetSeen",""),"UTF-8")+"&"+
                        URLEncoder.encode("o18","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("standardLiving",""),"UTF-8")+"&"+
                        URLEncoder.encode("o19","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("entryPermitted",""),"UTF-8")+"&"+
                        URLEncoder.encode("o20","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("politicalPartyAffiliation",""),"UTF-8")+"&"+
                        URLEncoder.encode("o21","UTF-8") + "=" + URLEncoder.encode(ObservationShared.getString("routeMap",""),"UTF-8")+"&"+
                        //NeighbourFeedback
                        URLEncoder.encode("n1a","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("nameAndAddress1",""),"UTF-8")+"&"+
                        URLEncoder.encode("n1b","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("applicantStayAtGivenAddress1",""),"UTF-8")+"&"+
                        URLEncoder.encode("n1c","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("numberOfYearsBeenStaying1",""),"UTF-8")+"&"+
                        URLEncoder.encode("n1d","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("ownershipStatus1",""),"UTF-8")+"&"+
                        URLEncoder.encode("n2a","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("nameAndAddress2",""),"UTF-8")+"&"+
                        URLEncoder.encode("n2b","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("applicantStayAtGivenAddress2",""),"UTF-8")+"&"+
                        URLEncoder.encode("n2c","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("numberOfYearsBeenStaying2",""),"UTF-8")+"&"+
                        URLEncoder.encode("n2d","UTF-8") + "=" + URLEncoder.encode(neighbourShared.getString("ownershipStatus2",""),"UTF-8")+"&"+
                        //other details residence
                        URLEncoder.encode("od1","UTF-8") + "=" + URLEncoder.encode(od1,"UTF-8")+"&"+
                        URLEncoder.encode("od2","UTF-8") + "=" + URLEncoder.encode(od2,"UTF-8")+"&"+
                        //extra variables
                        URLEncoder.encode("SP","UTF-8") + "=" + URLEncoder.encode("","UTF-8")+"&"+
                        URLEncoder.encode("SEP","UTF-8") + "=" + URLEncoder.encode("","UTF-8");

                BW.write(data);
                BW.flush();
                BW.close();
                OS.close();

                InputStream IS = httpURLConnection.getInputStream();

                Log.d("IS","IS reached");
                BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
                Log.d("BR","BR reached");
                StringBuilder sb = new StringBuilder();

                while ((JSON_STRING = BR.readLine())!=null){
                    sb.append(JSON_STRING+"\n");
                }
//                String id = ""+personalDetailsShared.getString("id","") + personalDetailsShared.getString("username","");
                String jsn = sb.toString().trim();
                Log.d("jsn",jsn);
                BR.close();
                IS.close();

                httpURLConnection.disconnect();

                return  jsn;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            String responseStatus = "";
            try {
                JSONObject jsonObject = new JSONObject(result);
                responseStatus = jsonObject.getString("Response");
                if(responseStatus.equals("Success")){
                    fiResidentialSubmitCallBack.onSuccess("success");
                    Toast.makeText(OtherDetailsResidence.this,"Submitted Successfully.",Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(OtherDetailsResidence.this,"Something went wrong=="+result,Toast.LENGTH_LONG).show();
                }
        } catch (JSONException e) {
                e.printStackTrace();
        }catch(NullPointerException npe){
                Toast.makeText(OtherDetailsResidence.this,result,Toast.LENGTH_LONG).show();

        }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class FiDataFetchAsyncTaskAfterSubmit extends AsyncTask<String,Void,String> {
        String json_getUrl = "";
        String username = "";
        String JSON_STRING = "";
        Context activityContext = null;
        ProgressDialog progressDialog;

        FiDataFetchAsyncTaskAfterSubmit(Context activityContext,String username){
            this.activityContext = activityContext;
            this.username = username;
        }

        @Override
        protected void onPreExecute() {
            json_getUrl = "http://www.nscs.in/api/fi_data";
            progressDialog = new ProgressDialog(activityContext);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Please Wait");
//            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {

            try {
                String username = params[0];

                URL loginURL = new URL(json_getUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();

                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setAllowUserInteraction(false);
                httpURLConnection.setRequestProperty("username",username);

                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));

                String data = URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");

                BW.write(data);
                BW.flush();
                BW.close();
                OS.close();

                InputStream IS = httpURLConnection.getInputStream();

                Log.d("IS","IS reached");
                BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
                Log.d("BR","BR reached");
                StringBuilder sb = new StringBuilder();

                while ((JSON_STRING = BR.readLine())!=null){
                    sb.append(JSON_STRING+"\n");
                }

                String jsn = sb.toString().trim();
                Log.d("jsn",jsn);
                BR.close();
                IS.close();

                httpURLConnection.disconnect();

                return  jsn;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();}
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
//            progressDialog.dismiss();

            String responseStatus = "";
            try {
                JSONObject jsonObject = new JSONObject(result);
                responseStatus = jsonObject.getString("Response");
                if(responseStatus.equals("Success")){
                    Intent fi = new Intent(activityContext, FiDataActivity.class);
                    fi.putExtra("fiData",result);
                    fi.putExtra("username",this.username);
                    fi.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activityContext.startActivity(fi);
                }else{
                    Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(activityContext,"Server Error",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
}
