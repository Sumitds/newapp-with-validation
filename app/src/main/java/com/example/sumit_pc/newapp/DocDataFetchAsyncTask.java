package com.example.sumit_pc.newapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.DocDataActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by sumit-pc on 21/11/17.
 */

public class DocDataFetchAsyncTask extends AsyncTask<String,Void,String> {
    String json_getUrl = "";
    String username = "";
    String JSON_STRING = "";
    Context activityContext = null;
    ProgressDialog progressDialog;

    DocDataFetchAsyncTask(Context activityContext,String username){
        this.activityContext = activityContext;
        this.username = username;
    }

    @Override
    protected void onPreExecute() {
        json_getUrl = "http://www.nscs.in/api/doc_data";
        progressDialog = new ProgressDialog(activityContext);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {

        try {
            String username = params[0];

            URL loginURL = new URL(json_getUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setAllowUserInteraction(false);
            httpURLConnection.setRequestProperty("username",username);

            OutputStream OS = httpURLConnection.getOutputStream();
            BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));

            String data = URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");

            BW.write(data);
            BW.flush();
            BW.close();
            OS.close();

            InputStream IS = httpURLConnection.getInputStream();

            Log.d("IS","IS reached");
            BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
            Log.d("BR","BR reached");
            StringBuilder sb = new StringBuilder();

            while ((JSON_STRING = BR.readLine())!=null){
                sb.append(JSON_STRING+"\n");
            }

            String jsn = sb.toString().trim();
            Log.d("jsn",jsn);
            BR.close();
            IS.close();

            httpURLConnection.disconnect();

            return  jsn;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();}
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();

        String responseStatus = "";
        try {
            JSONObject jsonObject = new JSONObject(result);
            responseStatus = jsonObject.getString("Response");
            if(responseStatus.equals("Success")){
                Intent doc = new Intent(activityContext, DocDataActivity.class);
                doc.putExtra("docData",result);
                doc.putExtra("username",this.username);
                activityContext.startActivity(doc);
            }else{
                Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
            }

    } catch (Exception e) {
            Toast.makeText(activityContext,"Server Error",Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
