package com.example.sumit_pc.newapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by sumit-pc on 25/11/17.
 */

public class DocDataSubmitAsyncTask extends AsyncTask<String, Void, String> {

    String username="";
    String status="";
    String id="";
    String remark="";
    String json_getUrl ="";
    String JSON_STRING ="";
    ProgressDialog progressDialog;
    Context activityContext;
    SubmitCallBack submitCallBack;

    DocDataSubmitAsyncTask(Context activityContext, SubmitCallBack submitCallBack){
        this.activityContext = activityContext;
        this.submitCallBack = submitCallBack;

    }
    @Override
    protected void onPreExecute() {
        json_getUrl = "http://www.nscs.in/api/doc_submit";
        progressDialog = new ProgressDialog(activityContext);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {

        try {
            this.username = params[0];
            this.status = params[1];
            this.id = params[2];
            this.remark = params[3];

            URL loginURL = new URL(json_getUrl);
            Log.d("loginURL","loginURL reached");
            HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();
            Log.d("httpconn","httpconn reached");

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setAllowUserInteraction(false);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpURLConnection.setRequestProperty("username",this.username);
            httpURLConnection.setRequestProperty("status",this.status);
            httpURLConnection.setRequestProperty("id",this.id);
            httpURLConnection.setRequestProperty("remark",this.remark);

            OutputStream OS = httpURLConnection.getOutputStream();
            BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = "";
            try {
                data =  URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(this.username,"UTF-8")+"&"+
                        URLEncoder.encode("status","UTF-8") + "=" + URLEncoder.encode(this.status,"UTF-8")+"&"+
                        URLEncoder.encode("id","UTF-8") + "=" + URLEncoder.encode(this.id,"UTF-8")+"&"+
                        URLEncoder.encode("remarks","UTF-8") + "=" + URLEncoder.encode(this.remark,"UTF-8");

            }catch (NullPointerException npe){
                npe.printStackTrace();
            }

            BW.write(data);
            BW.flush();
            BW.close();
            OS.close();

            InputStream IS = httpURLConnection.getInputStream();

            Log.d("IS","IS reached");
            BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
            Log.d("BR","BR reached");
            StringBuilder sb = new StringBuilder();

            while ((JSON_STRING = BR.readLine())!=null){
                sb.append(JSON_STRING+"\n");
            }

            String jsn = sb.toString().trim();
            Log.d("jsn",jsn);
            BR.close();
            IS.close();

            httpURLConnection.disconnect();

            return  jsn;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();}
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();
        String responseStatus = "";
        try {
            JSONObject jsonObject = new JSONObject(result);
            responseStatus = jsonObject.getString("Response");
            if(responseStatus.equals("Success")){
                submitCallBack.onSuccess("success"+id);
            }else{
                Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
