package com.example.sumit_pc.newapp;

/**
 * Created by sumit-pc on 30/11/17.
 */

public interface FiResidentialSubmitCallBack {
   void onSuccess(String success);
}
