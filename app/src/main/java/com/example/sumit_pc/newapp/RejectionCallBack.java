package com.example.sumit_pc.newapp;

/**
 * Created by sumit-pc on 23/11/17.
 */

public interface RejectionCallBack {
    void onSuccess (String status);
}
