package com.example.sumit_pc.newapp.officeverification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.ToastMsg;

public class SelfEmployedProfileActivity extends AppCompatActivity {

    final String selfEmployedDetailsSharedPref = "SelfEmployedDetails";
    Spinner businessOwnership, businessPremises;
    ArrayAdapter<CharSequence> businessOwnershipAdapter, businessPremisesAdapter;
    Button nextNeighbourCheckActivity;
    RadioGroup businessProof;
    RadioButton businessProofYes, businessProofNo;
    EditText personMet, natureOfBusiness, businessBoardSeen, exteriorPremises, interiorPremises, totalNoEmployee,
            numberOfEmployeeSeen, numberOfCustomerSeen, avgBillingPerCustomer, numberOfCustomerPerDay, avgMonthlyTurnOver,
            stockAndMachinery, peakBusinessHours, marketHoliday, levelOfBusinessActivity,  vehicleRegistrationNumber,
            existingVehicleDetails, vehicleFreeOrFinance, existingLoanOblogation, affiliationToPolitics;
    SharedPreferences sharedpreferences ;
    SharedPreferences.Editor editor ;
    String businessProofAvailable;

    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_employed_profile);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        personMet = (EditText) findViewById(R.id.person_met_and_relationship);
        natureOfBusiness = (EditText) findViewById(R.id.nature_of_business);
        businessBoardSeen = (EditText) findViewById(R.id.business_board_seen);
        exteriorPremises = (EditText) findViewById(R.id.exteriors_of_business_premises);
        interiorPremises = (EditText) findViewById(R.id.interiors_of_business_premises);
        totalNoEmployee = (EditText) findViewById(R.id.total_number_of_employee_self_employed);
        numberOfEmployeeSeen = (EditText) findViewById(R.id.number_of_emp_seen);
        numberOfCustomerSeen = (EditText) findViewById(R.id.number_of_customer_seen);
        avgBillingPerCustomer = (EditText) findViewById(R.id.average_billing_per_customer);
        numberOfCustomerPerDay = (EditText) findViewById(R.id.number_of_customers_per_day);
        avgMonthlyTurnOver = (EditText) findViewById(R.id.average_monthly_turnover);
        stockAndMachinery = (EditText) findViewById(R.id.stock_and_machinery_details);
        peakBusinessHours = (EditText) findViewById(R.id.peak_business_hours);
        marketHoliday = (EditText) findViewById(R.id.market_holiday);
        levelOfBusinessActivity = (EditText) findViewById(R.id.level_of_business_activity_self_employed);
        vehicleRegistrationNumber = (EditText) findViewById(R.id.vehicle_registration_number);
        existingVehicleDetails = (EditText) findViewById(R.id.existing_vehicle_details);
        vehicleFreeOrFinance = (EditText) findViewById(R.id.vehicle_free_or_finance);
        existingLoanOblogation = (EditText) findViewById(R.id.existing_loan_obligations);
        affiliationToPolitics = (EditText) findViewById(R.id.political_party_affiliation);
        businessProof = (RadioGroup) findViewById(R.id.business_proof_verified);
        businessProofYes = (RadioButton) findViewById(R.id.business_proof_verified_yes);
        businessProofNo = (RadioButton) findViewById(R.id.business_proof_verified_no);


        businessOwnership = (Spinner) findViewById(R.id.business_ownership);
        businessOwnershipAdapter = ArrayAdapter.createFromResource(this,
                R.array.business_ownership_options, android.R.layout.simple_spinner_item);
        businessOwnershipAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        businessOwnership.setAdapter(businessOwnershipAdapter);

        businessPremises = (Spinner) findViewById(R.id.business_premises);
        businessPremisesAdapter = ArrayAdapter.createFromResource(this,
                R.array.business_premises_options, android.R.layout.simple_spinner_item);
        businessPremisesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        businessPremises.setAdapter(businessPremisesAdapter);

        nextNeighbourCheckActivity = (Button) findViewById(R.id.next_neighbour_check);


        sharedpreferences = getSharedPreferences(selfEmployedDetailsSharedPref, Context.MODE_PRIVATE);
        nextNeighbourCheckActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastMsg toastMsg = new ToastMsg(SelfEmployedProfileActivity.this);
                if(businessProofYes.isChecked() || businessProofNo.isChecked()){
                    if(businessProofYes.isChecked()){
                        businessProofAvailable = businessProofYes.getText().toString();
                    }else if(businessProofNo.isChecked()){
                        businessProofAvailable = businessProofNo.getText().toString();
                    }
                    if(toastMsg.isContainSpecialChar(personMet.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(natureOfBusiness.getText().toString())
                            && toastMsg.isContainSpecialChar(peakBusinessHours.getText().toString())){
                        storeSelfEmplyedDetails();
                        Intent next = new Intent(SelfEmployedProfileActivity.this, NeighbourCheck.class);
                        next.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(next);
                    }else {
                        toastMsg.showToast("No special character or emoticon allowed.");

                    }

                }else if(!businessProofYes.isChecked() || !businessProofNo.isChecked()) {
                    businessProofAvailable = "";
                    if (toastMsg.isContainSpecialChar(personMet.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(natureOfBusiness.getText().toString())
                            && toastMsg.isContainSpecialChar(peakBusinessHours.getText().toString())) {
                        storeSelfEmplyedDetails();
                        Intent next = new Intent(SelfEmployedProfileActivity.this, NeighbourCheck.class);
                        next.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(next);
                    } else {
                        toastMsg.showToast("No special character or emoticon allowed.");

                    }
                }
            }
        });
    }

    void storeSelfEmplyedDetails(){
        editor = sharedpreferences.edit();
        editor.clear();
        editor.putString("personMet",personMet.getText().toString());
        editor.putString("natureOfBusiness",natureOfBusiness.getText().toString());
        editor.putString("businessBoardSeen",businessBoardSeen.getText().toString());
        editor.putString("businessProof",businessProofAvailable);
        editor.putString("businessOwnership",businessOwnership.getSelectedItem().toString());
        editor.putString("businessPremises",businessPremises.getSelectedItem().toString());
        editor.putString("exteriorPremises",exteriorPremises.getText().toString());
        editor.putString("interiorPremises",interiorPremises.getText().toString());
        editor.putString("totalNoEmployee",totalNoEmployee.getText().toString());
        editor.putString("numberOfEmployeeSeen",numberOfEmployeeSeen.getText().toString());
        editor.putString("numberOfCustomerSeen",numberOfCustomerSeen.getText().toString());
        editor.putString("avgBillingPerCustomer",avgBillingPerCustomer.getText().toString());
        editor.putString("numberOfCustomerPerDay",numberOfCustomerPerDay.getText().toString());
        editor.putString("avgMonthlyTurnOver",avgMonthlyTurnOver.getText().toString());
        editor.putString("stockAndMachinery",stockAndMachinery.getText().toString());
        editor.putString("peakBusinessHours",peakBusinessHours.getText().toString());
        editor.putString("marketHoliday",marketHoliday.getText().toString());
        editor.putString("levelOfBusinessActivity",levelOfBusinessActivity.getText().toString());
        editor.putString("existingVehicleDetails",existingVehicleDetails.getText().toString());
        editor.putString("vehicleRegistrationNumber",vehicleRegistrationNumber.getText().toString());
        editor.putString("vehicleFreeOrFinance",vehicleFreeOrFinance.getText().toString());
        editor.putString("existingLoanOblogation",existingLoanOblogation.getText().toString());
        editor.putString("affiliationToPolitics",affiliationToPolitics.getText().toString());
        editor.commit();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
