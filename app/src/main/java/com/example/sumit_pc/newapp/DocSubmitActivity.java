package com.example.sumit_pc.newapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


/**
 * A login screen that offers login via email/password.
 */
public class DocSubmitActivity extends AppCompatActivity {

    RadioGroup statusDocSubmit;
    RadioButton positiveRadio;
    RadioButton negativeRadio;
    RadioButton declinedRadio;
    RadioButton refferedRadio;
    Button docSubmitBtn;
    EditText remarkEditTxt;
    String user="";
    String caseId="";
    String status="";
    String remark = "";
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_submit);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        positiveRadio = (RadioButton) findViewById(R.id.docPositive);
        negativeRadio = (RadioButton) findViewById(R.id.docNegative);
        declinedRadio = (RadioButton) findViewById(R.id.docDeclined);
        refferedRadio = (RadioButton) findViewById(R.id.docReffered);
        docSubmitBtn = (Button) findViewById(R.id.docSubmit);
        remarkEditTxt = (EditText) findViewById(R.id.remark_doc_submit);
        statusDocSubmit = (RadioGroup) findViewById(R.id.status_Doc_Submit);
        this.user = getIntent().getStringExtra("username");
        this.caseId = getIntent().getStringExtra("caseId");
        docSubmitBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                remark = remarkEditTxt.getText().toString().trim();
            if(positiveRadio.isChecked() || negativeRadio.isChecked() || declinedRadio.isChecked() || refferedRadio.isChecked()){
                switch (statusDocSubmit.getCheckedRadioButtonId()){
                    case R.id.docPositive:
                        status = positiveRadio.getText().toString();
                        break;
                    case R.id.docNegative:
                        status = negativeRadio.getText().toString();
                        break;
                    case R.id.docDeclined:
                        status = declinedRadio.getText().toString();
                        break;
                    case R.id.docReffered:
                        status = refferedRadio.getText().toString();
                        break;
                }
                if(!remark.equals("")){
                    DocDataSubmitAsyncTask docDataSubmitAsyncTask = new DocDataSubmitAsyncTask(DocSubmitActivity.this, new SubmitCallBack() {
                        @Override
                        public void onSuccess(String status) {
                            if(status.equals("success"+caseId)){
                                Toast.makeText(DocSubmitActivity.this,"Doc Submit Successful", Toast.LENGTH_SHORT).show();
                                DocDataFetchAsyncTaskAfterSubmit docDataFetchAsyncTaskAfterSubmit = new DocDataFetchAsyncTaskAfterSubmit(DocSubmitActivity.this, user);
                                docDataFetchAsyncTaskAfterSubmit.execute(user);
//                                finish();
                            }
                        }
                    });
                    docDataSubmitAsyncTask.execute(user, status, caseId, remark);
                }else{
                    Toast.makeText(DocSubmitActivity.this,"remark should not be empty",Toast.LENGTH_SHORT).show();
                    remarkEditTxt.setText("");
                }
            }else{
                Toast.makeText(DocSubmitActivity.this,"Please choose a status",Toast.LENGTH_SHORT).show();
            }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class DocDataFetchAsyncTaskAfterSubmit extends AsyncTask<String,Void,String> {
        String json_getUrl = "";
        String username = "";
        String JSON_STRING = "";
        Context activityContext = null;
        ProgressDialog progressDialog;

        DocDataFetchAsyncTaskAfterSubmit(Context activityContext,String username){
            this.activityContext = activityContext;
            this.username = username;
        }

        @Override
        protected void onPreExecute() {
            json_getUrl = "http://www.nscs.in/api/doc_data";
            progressDialog = new ProgressDialog(activityContext);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Please Wait");
            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {

            try {
                String username = params[0];

                URL loginURL = new URL(json_getUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();

                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setAllowUserInteraction(false);
                httpURLConnection.setRequestProperty("username",username);

                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));

                String data = URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");

                BW.write(data);
                BW.flush();
                BW.close();
                OS.close();

                InputStream IS = httpURLConnection.getInputStream();

                Log.d("IS","IS reached");
                BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
                Log.d("BR","BR reached");
                StringBuilder sb = new StringBuilder();

                while ((JSON_STRING = BR.readLine())!=null){
                    sb.append(JSON_STRING+"\n");
                }

                String jsn = sb.toString().trim();
                Log.d("jsn",jsn);
                BR.close();
                IS.close();

                httpURLConnection.disconnect();

                return  jsn;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();}
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();

            String responseStatus = "";
            try {
                JSONObject jsonObject = new JSONObject(result);
                responseStatus = jsonObject.getString("Response");
                if(responseStatus.equals("Success")){
                    Intent doc = new Intent(activityContext, DocDataActivity.class);
                    doc.putExtra("docData",result);
                    doc.putExtra("username",this.username);
                    doc.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activityContext.startActivity(doc);
                }else{
                    Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(activityContext,"Server Error",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
}
