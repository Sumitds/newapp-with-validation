package com.example.sumit_pc.newapp;

/**
 * Created by sumit-pc on 25/11/17.
 */

public interface SubmitCallBack {
    void onSuccess(String status);
}
