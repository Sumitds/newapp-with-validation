package com.example.sumit_pc.newapp;

import java.util.List;

/**
 * Created by sumit-pc on 20/10/17.
 */

public class FiDataParser {


    /**
     * Response : Success
     * Data : [{"case_id":2,"client_name":"hdfc2","product_name":"Home loan","customer_name":"dsadasda","doc_name":null,"dept_name":"dasdasdas","dept_address":null},{"case_id":3,"client_name":"hdfc2","product_name":"Home loan","customer_name":"saddasd","doc_name":"0","dept_name":"dsadasd","dept_address":"dsadasdasdwq"},{"case_id":4,"client_name":"hdfc2","product_name":"Home loan","customer_name":"dasdsad","doc_name":"0","dept_name":"dasdas","dept_address":"sdadsadasdsddsa"}]
     */

    private List<FiDataParser.DataBean> Data;

    public List<FiDataParser.DataBean> getData() {
        return Data;
    }

    public void setData(List<FiDataParser.DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * case_id : 2
         * client_name : hdfc2
         * product_name : Home loan
         * customer_name : dsadasda
         * doc_name : null
         * dept_name : dasdasdas
         * dept_address : null
         */


        private String case_id;
        private String client_name;
        private String product_name;
        private String customer_name;
        private String verification_type;
        private String office_or_home_address;
        private String username;

        DataBean(String case_id,String client_name,
                 String product_name,String customer_name,
                 String verification_type,String office_or_home_address){
            this.case_id = case_id;
            this.client_name = client_name;
            this.product_name = product_name;
            this.customer_name = customer_name;
            this.verification_type = verification_type;
            this.office_or_home_address = office_or_home_address;
        }

        DataBean(){}
        DataBean(String username){
            this.username = username;
        }

        public String getCase_id() {
            return case_id;
        }

        public void setCase_id(String case_id) {
            this.case_id = case_id;
        }

        public String getClient_name() {
            return client_name;
        }

        public void setClient_name(String client_name) {
            this.client_name = client_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getVerification_type() {
            return verification_type;
        }

        public void setVerification_type(String verification_type) {
            this.verification_type = verification_type;
        }

        public String getOffice_or_home_address() {
            return office_or_home_address;
        }

        public void setOffice_or_home_address(String office_or_home_address) {
            this.office_or_home_address = office_or_home_address;
        }
        public void setUsername(String username){
            this.username = username;
        }
        public String getUsername(){
            return this.username;
        }


    }
}
