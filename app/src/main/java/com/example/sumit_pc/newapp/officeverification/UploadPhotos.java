package com.example.sumit_pc.newapp.officeverification;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;

public class UploadPhotos extends AppCompatActivity {


    ImageView img1, img2, img3, img4, img5, img6, img7;
    Button uplaodImages;
    String exception ="";

    Bitmap image1, image2, image3, image4, image5, image6, image7;

    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE1 = 101;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE2 = 102;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE3 = 103;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE4 = 104;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE5 = 105;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE6 = 106;
    String id,imageUser1,imageUser2,imageUser3,imageUser4,imageUser5,imageUser6,imageUser7;
    String username, JSON_STRING;
    final String uploadPhotos = "UploadPhotos";
    SharedPreferences sharedPreferences, getUserSharedPref;
    SharedPreferences.Editor editor;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_photos);


        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        img1 = (ImageView) findViewById(R.id.img1_fi_submit);
        img2 = (ImageView) findViewById(R.id.img2_fi_submit);
        img3 = (ImageView) findViewById(R.id.img3_fi_submit);
        img4 = (ImageView) findViewById(R.id.img4_fi_submit);
        img5 = (ImageView) findViewById(R.id.img5_fi_submit);
        img6 = (ImageView) findViewById(R.id.img6_fi_submit);
        img7 = (ImageView) findViewById(R.id.img7_fi_submit);
        uplaodImages = (Button) findViewById(R.id.uplaod_photo);


        byte[] byteArray = getIntent().getByteArrayExtra("image");
//        id = getIntent().getStringExtra("id");
//        username = getIntent().getStringExtra("username");
//        ConvertImage[0] = Base64.encodeToString(byteArray, Base64.DEFAULT);
        try {
            image1 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imageUser1 = ConvertBitmapToString(image1);
            img1.setImageBitmap(image1);
        }catch (NullPointerException npe){

        }
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadPhotos.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadPhotos.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE1);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE1);
                    // permission has been granted, continue as usual

                }


            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadPhotos.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadPhotos.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE2);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE2);
                    // permission has been granted, continue as usual

                }
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadPhotos.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadPhotos.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE3);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE3);
                    // permission has been granted, continue as usual

                }
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadPhotos.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadPhotos.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE4);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE4);
                    // permission has been granted, continue as usual

                }
            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadPhotos.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadPhotos.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE5);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE5);
                    // permission has been granted, continue as usual

                }
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadPhotos.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Check Permissions Now
                    ActivityCompat.requestPermissions(UploadPhotos.this,
                            new String[]{Manifest.permission.CAMERA},
                            UploadPhotos.CAMERA_CAPTURE_IMAGE_REQUEST_CODE6);
                } else {
                    Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(captureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE6);
                    // permission has been granted, continue as usual

                }
            }
        });



        sharedPreferences = getSharedPreferences(uploadPhotos, Context.MODE_PRIVATE);
        getUserSharedPref = getSharedPreferences("FiDataUser", Context.MODE_PRIVATE);

        uplaodImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = getUserSharedPref.getString("caseId","");
                username = getUserSharedPref.getString("username","");

                editor = sharedPreferences.edit();
                editor.clear();
                editor.putString("caseId", id);
                editor.putString("username",username);
                editor.commit();

                UploadImageFiOffice uploadImageFiOffice = new UploadImageFiOffice();
//                uploadImageFiOffice.execute("http://192.168.0.122/api/photoInsert.php");
                uploadImageFiOffice.execute("http://www.nscs.in/api/img_submit");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                image2 = (Bitmap) data.getExtras().get("data");
                imageUser2 = ConvertBitmapToString(image2);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image2.compress(Bitmap.CompressFormat.PNG, 100, stream);

                img2.setImageBitmap(image2);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 102) {
            if (resultCode == RESULT_OK) {
                image3 = (Bitmap) data.getExtras().get("data");
                imageUser3 = ConvertBitmapToString(image3);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image3.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img3.setImageBitmap(image3);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 103) {
            if (resultCode == RESULT_OK) {
                image4 = (Bitmap) data.getExtras().get("data");
                imageUser4 = ConvertBitmapToString(image4);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image4.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img4.setImageBitmap(image4);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 104) {
            if (resultCode == RESULT_OK) {
                image5 = (Bitmap) data.getExtras().get("data");
                imageUser5 = ConvertBitmapToString(image5);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image5.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img5.setImageBitmap(image5);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 105) {
            if (resultCode == RESULT_OK) {
                image6 = (Bitmap) data.getExtras().get("data");
                imageUser6 = ConvertBitmapToString(image6);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image6.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img6.setImageBitmap(image6);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if (requestCode == 106) {
            if (resultCode == RESULT_OK) {
                image7 = (Bitmap) data.getExtras().get("data");
                imageUser7 = ConvertBitmapToString(image7);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image7.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                img7.setImageBitmap(image7);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }


    }

    private class UploadImageFiOffice extends AsyncTask<String, Void, String> {

        private String Content;
        private String Error = null;
        String data = "";
        private BufferedReader reader;
        ProgressDialog pDialog;


        protected void onPreExecute() {
            pDialog = new ProgressDialog(UploadPhotos.this);
            pDialog.setCancelable(false);
            pDialog.setTitle("Uploading...");
            pDialog.setMessage("Please Wait");
            pDialog.show();

        }

        protected String doInBackground(String... urls) {

//            HttpURLConnection connection = null;
            try {
                URI uri = new URI(urls[0]);
                String urlString = uri.toString();
                URL url = new URL(urlString);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                con.setRequestMethod("POST");
                con.setUseCaches(false);
//                con.setDoInput(true);
//                con.setDoOutput(true);
                con.setConnectTimeout(15000);
                con.setReadTimeout(20000);
//                con.setRequestProperty("Content-Length", "" + data.getBytes().length);
                con.setRequestProperty("Connection", "Keep-Alive");
                con.setRequestProperty("id", id);
                con.setRequestProperty("username", username);
                /*con.setRequestProperty("img1", imageUser1);
                con.setRequestProperty("img2", imageUser2);
                con.setRequestProperty("img3", imageUser3);
                con.setRequestProperty("img4", imageUser4);
                con.setRequestProperty("img5", imageUser5);
                con.setRequestProperty("img6", imageUser6);
                con.setRequestProperty("img7", imageUser7);*/
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));


                try {

                    data += "&img1=data:image/png;base64," + URLEncoder.encode(imageUser1, "UTF-8");
                    if(imageUser2 != null && imageUser2.length() != 0) {
                        data += "&img2=data:image/png;base64," + URLEncoder.encode(imageUser2, "UTF-8");
                    }
                    if(imageUser3 != null && imageUser3.length() != 0) {
                        data += "&img3=data:image/png;base64," + URLEncoder.encode(imageUser3, "UTF-8");
                    }
                    if(imageUser4 != null && imageUser4.length() != 0) {
                        data += "&img4=data:image/png;base64," + URLEncoder.encode(imageUser4, "UTF-8");
                    }
                    if(imageUser5 != null && imageUser5.length() != 0) {
                        data += "&img5=data:image/png;base64," + URLEncoder.encode(imageUser5, "UTF-8");
                    }
                    if(imageUser6 != null && imageUser6.length() != 0) {
                        data += "&img6=data:image/png;base64," + URLEncoder.encode(imageUser6, "UTF-8");
                    }
                    if(imageUser7 != null && imageUser7.length() != 0) {
                        data += "&img7=data:image/png;base64," + URLEncoder.encode(imageUser7, "UTF-8");
                    }
                    data += "&" + URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+
                            "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");


//                    data += "&img1=data:image/png;base64," + URLEncoder.encode(imageUser1, "UTF-8")
//                            +"&" + URLEncoder.encode("img2", "UTF-8") + "=" + "data:image/png;base64," + imageUser2
//                            +"&" + URLEncoder.encode("img3", "UTF-8") + "=" + "data:image/png;base64," + imageUser3
//                            +"&" + URLEncoder.encode("img4", "UTF-8") + "=" + "data:image/png;base64," + imageUser4
//                            +"&" + URLEncoder.encode("img5", "UTF-8") + "=" + "data:image/png;base64," + imageUser5
//                            +"&" + URLEncoder.encode("img6", "UTF-8") + "=" + "data:image/png;base64," + imageUser6
//                            +"&" + URLEncoder.encode("img7", "UTF-8") + "=" + "data:image/png;base64," + imageUser7
//                            +"&img2=data:image/png;base64," + URLEncoder.encode(imageUser2, "UTF-8")
//                            +"&img3=data:image/png;base64," + URLEncoder.encode(imageUser3, "UTF-8")
//                            +"&img4=data:image/png;base64," + URLEncoder.encode(imageUser4, "UTF-8")
//                            +"&img5=data:image/png;base64," + URLEncoder.encode(imageUser5, "UTF-8")
//                            +"&img6=data:image/png;base64," + URLEncoder.encode(imageUser6, "UTF-8")
//                            +"&img7=data:image/png;base64," + URLEncoder.encode(imageUser7, "UTF-8")
//                            +"&" + URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+
//                            "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");



//                    data += "&img1=data:image/png;base64," + URLEncoder.encode(imageUser1, "UTF-8")
//                            +"&img2=data:image/png;base64," + URLEncoder.encode(imageUser2, "UTF-8")
//                            +"&img3=data:image/png;base64," + URLEncoder.encode(imageUser3, "UTF-8")
//                            +"&img4=data:image/png;base64," + URLEncoder.encode(imageUser4, "UTF-8")
//                            +"&img5=data:image/png;base64," + URLEncoder.encode(imageUser5, "UTF-8")
//                            +"&img6=data:image/png;base64," + URLEncoder.encode(imageUser6, "UTF-8")
//                            +"&img7=data:image/png;base64," + URLEncoder.encode(imageUser7, "UTF-8")
//                            +"&" + URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+
//                            "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                //make request
                writer.write(data);
                writer.flush();
                writer.close();
                int code = con.getResponseCode();
                InputStream is = null;
                if (code >= 200 && code < 400) {
                    // Create an InputStream in order to extract the response object
                    is = con.getInputStream();
                }
                else {
                    is = con.getErrorStream();
                }
                reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                exception = ""+code;

                Content = sb.toString();
                return Content;
            } catch (Exception ex) {
//                exception = ex.getMessage();
                Error = ex.getMessage().toString();
            }
            return null;

        }


        protected void onPostExecute(String unused) {
            // NOTE: You can call UI Element here.

            pDialog.dismiss();
            try {

                if (Content != null) {
                    JSONObject jsonResponse = new JSONObject(Content);
                    String status = jsonResponse.getString("Response");
                    if (status.equals("Success")) {
                        Toast.makeText(getApplicationContext(), "Uploaded Successfully.", Toast.LENGTH_SHORT).show();
                        Intent nextGeneralDetails = new Intent(UploadPhotos.this, GeneralDetailsFiSubmit.class);
                        startActivity(nextGeneralDetails);
                        finish();

                    } else {

                        Toast.makeText(getApplicationContext(), unused, Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public static String ConvertBitmapToString(Bitmap bitmap){
        String encodedImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        encodedImage= Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

        return encodedImage;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

