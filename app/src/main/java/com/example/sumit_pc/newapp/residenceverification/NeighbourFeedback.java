package com.example.sumit_pc.newapp.residenceverification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.ToastMsg;

public class NeighbourFeedback extends AppCompatActivity {

    Button nextOtherDetails;
    EditText nameAndAddress1, applicantStayAtGivenAddress1, numberOfYearsBeenStaying1, ownershipStatus1;
    EditText nameAndAddress2, applicantStayAtGivenAddress2, numberOfYearsBeenStaying2, ownershipStatus2;
    final String neighbourFeedBackSharedPreferences = "NeighbourFeedback";
    SharedPreferences sharedPreferences,ObservationShared;
    SharedPreferences.Editor editor;

    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neighbour_feedback);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        nameAndAddress1 = (EditText) findViewById(R.id.name_and_addr_neighbour1);
        nameAndAddress2 = (EditText) findViewById(R.id.name_and_addr_neighbour2);
        applicantStayAtGivenAddress1 = (EditText) findViewById(R.id.applicant_stay_given_addr_neighbour1);
        applicantStayAtGivenAddress2 = (EditText) findViewById(R.id.applicant_stay_given_addr_neighbour2);
        numberOfYearsBeenStaying1 = (EditText) findViewById(R.id.no_of_yrs_staying_neighbour1);
        numberOfYearsBeenStaying2 = (EditText) findViewById(R.id.no_of_yrs_staying_neighbour2);
        ownershipStatus1 = (EditText) findViewById(R.id.ownership_status_neighbour1);
        ownershipStatus2 = (EditText) findViewById(R.id.ownership_status_neighbour2);

        ObservationShared = getSharedPreferences("Observations", Context.MODE_PRIVATE);

        nextOtherDetails = (Button) findViewById(R.id.other_details_residence);
        sharedPreferences = getSharedPreferences(neighbourFeedBackSharedPreferences, Context.MODE_PRIVATE);
        nextOtherDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastMsg toastMsg = new ToastMsg(NeighbourFeedback.this);
                if(toastMsg.isContainSpecialChar(ownershipStatus1.getText().toString())
                   && toastMsg.isContainSpecialChar(ownershipStatus2.getText().toString())){
                    storeNeighbourFeedBack();
                    Intent otherDetals = new Intent(NeighbourFeedback.this, OtherDetailsResidence.class);
                    otherDetals.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(otherDetals);
                }else {
                    toastMsg.showToast("No special character or emoticon allowed.");

                }
            }
        });
    }

   void storeNeighbourFeedBack(){
       editor = sharedPreferences.edit();
       editor.clear();
       editor.putString("nameAndAddress1",nameAndAddress1.getText().toString());
       editor.putString("applicantStayAtGivenAddress1",applicantStayAtGivenAddress1.getText().toString());
       editor.putString("numberOfYearsBeenStaying1",numberOfYearsBeenStaying1.getText().toString());
       editor.putString("ownershipStatus1",ownershipStatus1.getText().toString());
       editor.putString("nameAndAddress2",nameAndAddress2.getText().toString());
       editor.putString("applicantStayAtGivenAddress2",applicantStayAtGivenAddress2.getText().toString());
       editor.putString("numberOfYearsBeenStaying2",numberOfYearsBeenStaying2.getText().toString());
       editor.putString("ownershipStatus2",ownershipStatus2.getText().toString());
       editor.commit();
   }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
