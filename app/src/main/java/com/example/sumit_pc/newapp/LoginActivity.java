package com.example.sumit_pc.newapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final int REQUEST_READ_CONTACTS = 0;
    String JSON_STRING ;
    String mEmail ="";
    String mPassword="";

    private UserLoginTask mAuthTask = null;
    String userShardPref="usernameStored";

    private AutoCompleteTextView username;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    SharedPreferences sharedPreferencesSession;
    SharedPreferences.Editor editorSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = (AutoCompleteTextView) findViewById(R.id.username);
        sharedPreferencesSession  = getSharedPreferences("Session",MODE_PRIVATE);
        editorSession = sharedPreferencesSession.edit();
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        if(!(sharedPreferencesSession.getString("username","").equals(""))){
            Intent dash = new Intent(LoginActivity.this,ProfileDashboardActivity.class);
            dash.putExtra("jsn",sharedPreferencesSession.getString("username",""));
            startActivity(dash);
            finish();
        }

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(username.getText().toString().equals("")) && !(mPasswordView.getText().toString().equals(""))){

                    if(CheckNetwork.isInternetAvailable(LoginActivity.this))
                    attemptLogin();
                    else{
                        Snackbar snackbar = Snackbar
                                .make(findViewById(R.id.coordinatorLayout), "No Internet Connection", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }else{
                    Toast.makeText(LoginActivity.this,"username / password can't be empty",Toast.LENGTH_LONG).show();
                }
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(username, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        }
        else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);

        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        username.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = username.getText().toString().trim();
        String password = mPasswordView.getText().toString().trim();


        final SharedPreferences sharedpreferences = this.getSharedPreferences(userShardPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.putString("username", email);
        editor.commit();

        boolean cancel = false;
        View focusView = null;
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if(!email.equals("")) {
                showProgress(true);

                mAuthTask = new UserLoginTask(email, password);
                mAuthTask.execute(email, password);
            }else{
                Toast.makeText(this,"username can't contain white spaces",Toast.LENGTH_LONG).show();
                username.setText("");
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        username.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<String,Void,String> {

        String json_getUrl;
        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected void onPreExecute() {
            json_getUrl = "http://www.nscs.in/api/login";
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            try {
                String email = params[0];
                String password = params[1];


                URL loginURL = new URL(json_getUrl);
                Log.d("loginURL","loginURL reached");
                HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();
                Log.d("httpconn","httpconn reached");

                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setAllowUserInteraction(false);
                httpURLConnection.setRequestProperty("username",email);
                httpURLConnection.setRequestProperty("password",password);

                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));

                String data = URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(email,"UTF-8")+"&"+
                        URLEncoder.encode("password","UTF-8") + "=" + URLEncoder.encode(password,"UTF-8");

                BW.write(data);
                BW.flush();
                BW.close();
                OS.close();

                InputStream IS = httpURLConnection.getInputStream();

                Log.d("IS","IS reached");
                BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
                Log.d("BR","BR reached");
                StringBuilder sb = new StringBuilder();

                while ((JSON_STRING = BR.readLine())!=null){
                    sb.append(JSON_STRING+"\n");
                }

                String jsn = sb.toString().trim();
                Log.d("jsn",jsn);
                BR.close();
                IS.close();

                httpURLConnection.disconnect();

                return  jsn;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // TODO: register the new account here.
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            JSONObject jsonObject = null;
            String response = "";
            String usr = "";
            mAuthTask = null;
            showProgress(false);
            try {
                jsonObject = new JSONObject(result);
                usr = jsonObject.getString("data");
                ToastMsg toastMsg = new ToastMsg(getApplicationContext());
                toastMsg.showToast("Logged in by "+usr);
            } catch (JSONException e) {
                e.printStackTrace();
            }catch (NullPointerException npe){
                Toast.makeText(LoginActivity.this,"Something went wrong",Toast.LENGTH_LONG).show();
            }finally {
                try {
                    response = jsonObject.getString("Response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (NullPointerException npe){
                    Toast.makeText(LoginActivity.this,"Something went wrong",Toast.LENGTH_LONG).show();

                }

            }

            if (!(usr.equals("")) && response.equals("success")) {
                editorSession.putString("username",usr);
                editorSession.putString("password",mPassword);
                editorSession.commit();
                Intent dash = new Intent(LoginActivity.this,ProfileDashboardActivity.class);
                dash.putExtra("jsn",usr);
                startActivity(dash);
                finish();
            } else if(response.equals("fail") || response.equals("")){
                username.setText("");
                mPasswordView.setText("");
                TextView responseStatus = (TextView) findViewById(R.id.responseStatus);
                responseStatus.setText("incorrect username / password!!");
                responseStatus.setTextColor(Color.RED);
                responseStatus.setError("");
                username.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

