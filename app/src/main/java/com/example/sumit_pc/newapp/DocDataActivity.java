package com.example.sumit_pc.newapp;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class DocDataActivity extends AppCompatActivity {

    JSONObject jsonObject;
    JSONArray jsonArray;
    JSONObject JO = null;
    ArrayList<DocDataParser.DataBean> docData;
    DocDataAdaptor adapter;
    DocDataParser.DataBean myPojo = null;
    TextView caseId,clientName,customerName, productName, docName,deptName,deptAddr, noDocData;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Intent content;
    String str_json;
    String username ="";
    ActionBar actionBar;
    private BroadcastReceiver broadcast_reciever;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewNew);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        caseId = (TextView) findViewById(R.id.case_id_doc);
        clientName = (TextView) findViewById(R.id.client_name_doc);
        productName = (TextView) findViewById(R.id.product_name_doc);
        customerName = (TextView) findViewById(R.id.customer_name_doc);
        docName = (TextView) findViewById(R.id.doc_name);
        deptName = (TextView) findViewById(R.id.dept_name_doc);
        deptAddr = (TextView) findViewById(R.id.dept_addr_doc);
        noDocData = (TextView) findViewById(R.id.no_doc_data);

        setTitle(R.string.doc_type);
        content = getIntent();
        //to be removed--------
        docData = new ArrayList<>();
        str_json = content.getStringExtra("docData");
        this.username = (String) getIntent().getCharSequenceExtra("username");
        DocDataParser.DataBean dataUser = new DocDataParser.DataBean(username);
        dataUser.setUsername(username);
        //---------

       try {
           jsonObject = new JSONObject(str_json);
           jsonArray = jsonObject.optJSONArray("Data");
           int j = jsonArray.length();
           if(j<=0){
               noDocData.setText("No Cases Available");
           }else{
               for (int i = 0; i < jsonArray.length(); i++) {
                   JSONObject post = jsonArray.optJSONObject(i);
                   DocDataParser.DataBean item = new DocDataParser.DataBean();
                   item.setCase_id(post.optString("case_id"));
                   item.setClient_name(post.optString("client_name"));
                   item.setProduct_name(post.optString("product_name"));
                   item.setCustomer_name(post.optString("customer_name"));
                   item.setDoc_name(post.getString("doc_name"));
                   item.setDept_name(post.getString("dept_name"));
                   item.setDept_address(post.getString("dept_address"));
                   item.setDownloadLink(post.getString("doc_link"));
                   docData.add(item);
               }
           }
       } catch (JSONException e) {
           e.printStackTrace();
       }catch (NullPointerException npe){}

           adapter = new DocDataAdaptor(docData,username, DocDataActivity.this);
           recyclerView.setAdapter(adapter);

           int count=0;
           while (count < jsonArray.length()) {
               try {
                   JO = jsonArray.getJSONObject(count);
                   myPojo = new DocDataParser.DataBean(
                           JO.getString("case_id"),JO.getString("client_name"),
                           JO.getString("product_name"),JO.getString("customer_name"),
                           JO.getString("doc_name"),JO.getString("dept_name"),JO.getString("dept_address")
                   );
               } catch (JSONException e) {
                   e.printStackTrace();
               }
               count++;
           }

        try{
            if(getDownloadAndWritePermission()){

            }
        }catch (Exception e){
            Toast.makeText(DocDataActivity.this,"Please Allow the app permissions",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    public  void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.d("MyAdapter", "onActivityResult");
//        if(requestCode == 500){
//            DocDataFetchAsyncTaskAfterSubmit fetchDocData = new DocDataFetchAsyncTaskAfterSubmit(DocDataActivity.this, username);
//            fetchDocData.execute(username);
//        }
//        finish();
////        Toast.makeText(this,"came here",Toast.LENGTH_LONG).show();
//    }

    private boolean getDownloadAndWritePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, 1);
        }
        else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, 1);
        }
        return false;
    }

//    public class DocDataFetchAsyncTaskAfterSubmit extends AsyncTask<String,Void,String> {
//        String json_getUrl = "";
//        String username = "";
//        String JSON_STRING = "";
//        Context activityContext = null;
//        ProgressDialog progressDialog;
//
//        DocDataFetchAsyncTaskAfterSubmit(Context activityContext,String username){
//            this.activityContext = activityContext;
//            this.username = username;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            json_getUrl = "http://www.nscs.in/api/doc_data";
//            progressDialog = new ProgressDialog(activityContext);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Loading...");
//            progressDialog.setMessage("Please Wait");
////            progressDialog.show();
//        }
//
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            try {
//                String username = params[0];
//
//                URL loginURL = new URL(json_getUrl);
//                HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();
//
//                httpURLConnection.setRequestMethod("POST");
//                httpURLConnection.setDoOutput(true);
//                httpURLConnection.setDoInput(true);
//                httpURLConnection.setUseCaches(false);
//                httpURLConnection.setAllowUserInteraction(false);
//                httpURLConnection.setRequestProperty("username",username);
//
//                OutputStream OS = httpURLConnection.getOutputStream();
//                BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
//
//                String data = URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");
//
//                BW.write(data);
//                BW.flush();
//                BW.close();
//                OS.close();
//
//                InputStream IS = httpURLConnection.getInputStream();
//
//                Log.d("IS","IS reached");
//                BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
//                Log.d("BR","BR reached");
//                StringBuilder sb = new StringBuilder();
//
//                while ((JSON_STRING = BR.readLine())!=null){
//                    sb.append(JSON_STRING+"\n");
//                }
//
//                String jsn = sb.toString().trim();
//                Log.d("jsn",jsn);
//                BR.close();
//                IS.close();
//
//                httpURLConnection.disconnect();
//
//                return  jsn;
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();}
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
////            progressDialog.dismiss();
//
//            String responseStatus = "";
//            try {
//                JSONObject jsonObject = new JSONObject(result);
//                responseStatus = jsonObject.getString("Response");
//                if(responseStatus.equals("Success")){
//                    Intent doc = new Intent(activityContext, DocDataActivity.class);
//                    doc.putExtra("docData",result);
//                    doc.putExtra("username",this.username);
//                    activityContext.startActivity(doc);
//                }else{
//                    Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
//                }
//
//            } catch (Exception e) {
//                Toast.makeText(activityContext,"Server Error",Toast.LENGTH_LONG).show();
//                e.printStackTrace();
//            }
//        }
//    }
}


