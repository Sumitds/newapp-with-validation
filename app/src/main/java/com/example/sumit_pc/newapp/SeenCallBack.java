package com.example.sumit_pc.newapp;

/**
 * Created by sumit-pc on 24/11/17.
 */

public interface SeenCallBack {
    void onSuccess (String status);
}
