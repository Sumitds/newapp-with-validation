package com.example.sumit_pc.newapp.officeverification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sumit_pc.newapp.R;

public class NeighbourCheck extends AppCompatActivity {

    Button moreDetails;
    EditText neighbourOne, neighbourTwo;
    final String neighbourCheckDetailsSharedPref = "NeighbourCheckDetails";
    SharedPreferences sharedpreferences ;
    SharedPreferences.Editor editor ;

    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neighbour_check);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        neighbourOne = (EditText) findViewById(R.id.neighbour_check1);
        neighbourTwo = (EditText) findViewById(R.id.neighbour_check2);
        moreDetails = (Button) findViewById(R.id.next_other_details);
        sharedpreferences = getSharedPreferences(neighbourCheckDetailsSharedPref, Context.MODE_PRIVATE);
        moreDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeNeighbourDetails();
                Intent nextOtherDetailsActivity = new Intent(NeighbourCheck.this, OtherDetails.class);
                nextOtherDetailsActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(nextOtherDetailsActivity);
            }
        });
    }

    void storeNeighbourDetails(){
        editor = sharedpreferences.edit();
        editor.clear();
        editor.putString("neighbourOne",neighbourOne.getText().toString());
        editor.putString("neighbourTwo",neighbourTwo.getText().toString());
        editor.commit();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
