package com.example.sumit_pc.newapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.officeverification.GeneralDetailsFiSubmit;
import com.example.sumit_pc.newapp.officeverification.UploadPhotos;
import com.example.sumit_pc.newapp.residenceverification.UploadImageResident;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by sumit-pc on 20/10/17.
 */

public class FiDataActivity extends AppCompatActivity {
    JSONObject jsonObject;
    JSONArray jsonArray;
    JSONObject JO = null;
    ArrayList<FiDataParser.DataBean> fiData;
    FiDataAdaptor adapter;
    FiDataParser.DataBean myPojo = null;
    TextView caseId,clientName,customerName, productType, officeOrHomeAddr, noFiData;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Intent content;
    String str_json ="";
    String username ="", userCaseId="";
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fi_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.fiRecyclerViewNew);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        caseId = (TextView) findViewById(R.id.case_id_fi);
        clientName = (TextView) findViewById(R.id.client_name_fi);
        productType = (TextView) findViewById(R.id.product_type_fi);
        customerName = (TextView) findViewById(R.id.customer_name_fi);
        officeOrHomeAddr = (TextView) findViewById(R.id.office_or_home_addr_fi);
        noFiData = (TextView) findViewById(R.id.no_fi_data);

        fiData = new ArrayList<>();
        content = getIntent();
        str_json = content.getStringExtra("fiData");
        this.username = (String) getIntent().getCharSequenceExtra("username");
        this.userCaseId = (String) getIntent().getCharSequenceExtra("caseId");
        FiDataParser.DataBean dataUser = new FiDataParser.DataBean(username);
        dataUser.setUsername(username);

        try {
            jsonObject = new JSONObject(str_json);
            jsonArray = jsonObject.optJSONArray("Data");
            int j = jsonArray.length();
            if(j<=0){
                noFiData.setText("No Cases Available");
            }else{
                for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject post = jsonArray.optJSONObject(i);
                FiDataParser.DataBean item = new FiDataParser.DataBean();
                item.setCase_id(post.optString("case_id"));
                item.setClient_name(post.optString("client_name"));
                item.setProduct_name(post.optString("product_name"));
                item.setCustomer_name(post.optString("customer_name"));
                item.setVerification_type(post.getString("verification_type"));
                item.setOffice_or_home_address(post.getString("address"));
                fiData.add(item);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter = new FiDataAdaptor(fiData,this.username, FiDataActivity.this);
        recyclerView.setAdapter(adapter);

        int count=0;

        while (count < jsonArray.length()) {
            try {
                JO = jsonArray.getJSONObject(count);
                myPojo = new FiDataParser.DataBean(
                        JO.getString("case_id"),JO.getString("client_name"),
                        JO.getString("product_name"),JO.getString("customer_name"),
                        JO.getString("verification_type"),JO.getString("address")
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }
            count++;
        }
        try{
        if(getCameraPermission()){

        }
        }catch (Exception e){
            Toast.makeText(FiDataActivity.this,"Please Allow the app permissions",Toast.LENGTH_LONG).show();
        }

    }

        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                Bitmap image = (Bitmap) data.getExtras().get("data");
                Intent sendPic = new Intent(this,UploadPhotos.class);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                sendPic.putExtra("image",byteArray);
                startActivity(sendPic);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }else if(requestCode == 200){
            if (resultCode == RESULT_OK) {
                Bitmap image = (Bitmap) data.getExtras().get("data");
                Intent sendPic = new Intent(this,UploadImageResident.class);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                sendPic.putExtra("image",byteArray);
                startActivity(sendPic);
            }else if (resultCode == RESULT_CANCELED)
            {Toast.makeText(this, "CANCELED ", Toast.LENGTH_LONG).show();}
        }
//        else if(requestCode == 400){
////            FiDataFetchAsyncTaskAfterSubmit fetchFiData = new FiDataFetchAsyncTaskAfterSubmit(FiDataActivity.this, username);
////            fetchFiData.execute(username);
//            finish();
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
        case android.R.id.home:
            this.finish();
            return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
    private boolean getCameraPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(CAMERA)) {

            requestPermissions(new String[]{CAMERA}, 100);

        }
        else {
            requestPermissions(new String[]{CAMERA}, 100);
        }
        return false;
    }
}
