package com.example.sumit_pc.newapp;

import java.util.List;

/**
 * Created by sumit-pc on 18/10/17.
 */

public class DocDataParser {

    /**
     * Response : Success
     * Data : [{"case_id":2,"client_name":"hdfc2","product_name":"Home loan","customer_name":"dsadasda","doc_name":null,"dept_name":"dasdasdas","dept_address":null},{"case_id":3,"client_name":"hdfc2","product_name":"Home loan","customer_name":"saddasd","doc_name":"0","dept_name":"dsadasd","dept_address":"dsadasdasdwq"},{"case_id":4,"client_name":"hdfc2","product_name":"Home loan","customer_name":"dasdsad","doc_name":"0","dept_name":"dasdas","dept_address":"sdadsadasdsddsa"}]
     */

    private List<DataBean> Data;

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * case_id : 2
         * client_name : hdfc2
         * product_name : Home loan
         * customer_name : dsadasda
         * doc_name : null
         * dept_name : dasdasdas
         * dept_address : null
         */


        private String case_id="";
        private String client_name="";
        private String product_name="";
        private String customer_name="";
        private String doc_name="";
        private String dept_name="";
        private String dept_address="";
        private String username="";

        private String downloadLink = "";
        DataBean(String case_id,String client_name,
                 String product_name,String customer_name,
                 String doc_name,String dept_name,String dept_address){
            this.case_id = case_id;
            this.client_name = client_name;
            this.product_name = product_name;
            this.customer_name = customer_name;
            this.doc_name = doc_name;
            this.dept_name = dept_name;
            this.dept_address = dept_address;
//            this.downloadLink = url;
        }
        DataBean(String username){
            this.username = username;
        }
        DataBean(){}

        public String getDownloadLink() {
            return downloadLink;
        }

        public void setDownloadLink(String downloadLink) {
            this.downloadLink = downloadLink;
        }

        public String getCase_id() {
            return case_id;
        }

        public void setCase_id(String case_id) {
            this.case_id = case_id;
        }

        public String getClient_name() {
            return client_name;
        }

        public void setClient_name(String client_name) {
            this.client_name = client_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getDept_name() {
            return dept_name;
        }

        public void setDept_name(String dept_name) {
            this.dept_name = dept_name;
        }

        public String getDept_address() {
            return dept_address;
        }

        public void setDept_address(String dept_address) {
            this.dept_address = dept_address;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
