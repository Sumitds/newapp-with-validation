package com.example.sumit_pc.newapp.officeverification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.ToastMsg;
import com.example.sumit_pc.newapp.residenceverification.NeighbourFeedback;
import com.example.sumit_pc.newapp.residenceverification.Observations;

public class SalariedProfileFiSubmit extends AppCompatActivity {


    EditText companyName, applicantDesignation,department, reportingTo, employeeNumber, yrsOfExperience, salaryDetails, totalNoOfEmployee,
    numberOfBranches, previousJobDetails;
    Spinner companyProfile, applicantJob;
    RadioButton transferableJobYes, transferableJobNo;
    RadioGroup transferableJobGroup;
    ArrayAdapter<CharSequence> companyProfileAdapter, applicantJobAdapter;
    Button nextSelfEmployedActivity;
    final String salariedSharedPref = "SalariedDetails";
    SharedPreferences sharedpreferences ;
    SharedPreferences.Editor editor ;
    String transferableJob;

    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salaried_profile_fi_submit);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        companyName = (EditText) findViewById(R.id.name_of_company);
        applicantDesignation = (EditText) findViewById(R.id.applicant_designation_salaried);
        department = (EditText) findViewById(R.id.dept_salaried);
        reportingTo = (EditText) findViewById(R.id.reporting_to_salaried);
        employeeNumber = (EditText) findViewById(R.id.employee_number_salaried);
        yrsOfExperience = (EditText) findViewById(R.id.yrs_of_exprnc);
        salaryDetails = (EditText) findViewById(R.id.salary_details_salaried);
        totalNoOfEmployee = (EditText) findViewById(R.id.total_number_of_emp);
        numberOfBranches = (EditText) findViewById(R.id.no_of_branches_salaried);
        previousJobDetails = (EditText) findViewById(R.id.previous_job_details_salaried);
        transferableJobYes = (RadioButton) findViewById(R.id.common_ofc_or_residence_yes);
        transferableJobNo = (RadioButton) findViewById(R.id.common_ofc_or_residence_no);

        companyProfile = (Spinner) findViewById(R.id.company_prfile_dropdown);
        companyProfileAdapter = ArrayAdapter.createFromResource(this,
                R.array.company_profile_options, android.R.layout.simple_spinner_item);
        companyProfileAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        companyProfile.setAdapter(companyProfileAdapter);

        applicantJob = (Spinner) findViewById(R.id.applicant_job_dropdown);
        applicantJobAdapter = ArrayAdapter.createFromResource(this,
                R.array.applicant_job_options, android.R.layout.simple_spinner_item);
        applicantJobAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        applicantJob.setAdapter(applicantJobAdapter);

        nextSelfEmployedActivity = (Button) findViewById(R.id.next_self_employed_button);

        sharedpreferences = getSharedPreferences(salariedSharedPref, Context.MODE_PRIVATE);

        nextSelfEmployedActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastMsg toastMsg = new ToastMsg(SalariedProfileFiSubmit.this);

                if(transferableJobYes.isChecked() || transferableJobNo.isChecked()){
                    if(transferableJobYes.isChecked()) {
                        transferableJob = transferableJobYes.getText().toString();
                    }else if(transferableJobNo.isChecked()){
                        transferableJob = transferableJobNo.getText().toString();
                    }
                    if(toastMsg.isContainSpecialCharOnly(applicantDesignation.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(department.getText().toString())
                            && toastMsg.isContainSpecialChar(reportingTo.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(salaryDetails.getText().toString())){
                        storeSalarieddetails();
                        Intent next = new Intent(SalariedProfileFiSubmit.this, SelfEmployedProfileActivity.class);
                        next.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(next);
                    }else {
                        toastMsg.showToast("No special character or emoticon allowed.");

                    }


                }else if(!transferableJobYes.isChecked() || !transferableJobNo.isChecked()){
                    transferableJob = "";
                    if(toastMsg.isContainSpecialCharOnly(applicantDesignation.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(department.getText().toString())
                            && toastMsg.isContainSpecialChar(reportingTo.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(salaryDetails.getText().toString())){
                        storeSalarieddetails();
                        Intent next = new Intent(SalariedProfileFiSubmit.this, SelfEmployedProfileActivity.class);
                        next.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(next);
                    }else {
                        toastMsg.showToast("No special character or emoticon allowed.");

                    }

                }

            }
        });
    }

    void storeSalarieddetails(){
        editor = sharedpreferences.edit();
        editor.clear();
        editor.putString("companyName",companyName.getText().toString());
        editor.putString("companyProfile",companyProfile.getSelectedItem().toString());
        editor.putString("applicantDesignation",applicantDesignation.getText().toString());
        editor.putString("department",department.getText().toString());
        editor.putString("reportingTo",reportingTo.getText().toString());
        editor.putString("employeeNumber",employeeNumber.getText().toString());
        editor.putString("applicantJob",applicantJob.getSelectedItem().toString());
        editor.putString("yrsOfExperience",yrsOfExperience.getText().toString());
        editor.putString("salaryDetails",salaryDetails.getText().toString());
        editor.putString("transferableJob",transferableJob);
        editor.putString("totalNoOfEmployee",totalNoOfEmployee.getText().toString());
        editor.putString("numberOfBranches",numberOfBranches.getText().toString());
        editor.putString("previousJobDetails",previousJobDetails.getText().toString());
        editor.commit();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
