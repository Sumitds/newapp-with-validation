package com.example.sumit_pc.newapp.officeverification;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringDef;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sumit_pc.newapp.FiDataActivity;
import com.example.sumit_pc.newapp.FiDataFetchAsyncTask;
import com.example.sumit_pc.newapp.FiResidentialSubmitCallBack;
import com.example.sumit_pc.newapp.ProfileDashboardActivity;
import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.residenceverification.OtherDetailsResidence;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class OtherDetails extends AppCompatActivity {

    CheckBox residenceCumOffice, nonTargetArea, outsideGeographic, entryNotAllowed, addrNotTraceable, applicantNotWorkingHere,
                detailMismatch, designationIncorrect, noBusinessActivity, negativeNeighbourCheck, badMarketRepo, otherReason;
    EditText verifiersRemark, additionalIncome;
    Button officeFiSubmit;

    ActionBar actionBar;
    SharedPreferences generalDetailsShared, neighbourCheckShared, salariedProfileShared, selfEmployedSared, fiDataActivityShared;

    final String generalSharedPref = "GeneralDetails";
    final String salariedSharedPref = "SalariedDetails";
    final String selfEmployedDetailsSharedPref = "SelfEmployedDetails";
    final String NeighbourCheckDetailsSharedPref = "NeighbourCheckDetails";
    final String fiDataUser = "UploadPhotos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_details);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        verifiersRemark = (EditText) findViewById(R.id.verifiers_remark);
        additionalIncome = (EditText) findViewById(R.id.additional_income_details);

        residenceCumOffice = (CheckBox) findViewById(R.id.residence_cum_ofc_checkbox);
        nonTargetArea = (CheckBox) findViewById(R.id.non_target_area_checkbox);
        outsideGeographic = (CheckBox) findViewById(R.id.outside_geographical_checkbox);
        entryNotAllowed = (CheckBox) findViewById(R.id.entry_not_allowed_checkbox);
        addrNotTraceable = (CheckBox) findViewById(R.id.address_not_traceable_checkbox);
        applicantNotWorkingHere = (CheckBox) findViewById(R.id.applicant_does_not_work_here_checkbox);
        detailMismatch = (CheckBox) findViewById(R.id.details_mismatch_checkbox);
        designationIncorrect = (CheckBox) findViewById(R.id.destination_incorrect_checkbox);
        noBusinessActivity = (CheckBox) findViewById(R.id.no_business_activity_checkbox);
        negativeNeighbourCheck = (CheckBox) findViewById(R.id.negative_neighbour_check_checkbox);
        badMarketRepo = (CheckBox) findViewById(R.id.bad_market_reputation_checkbox);
        otherReason = (CheckBox) findViewById(R.id.any_other_reason_checkbox);
        officeFiSubmit = (Button) findViewById(R.id.submit_final);

        officeFiSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otherDetailsValues = "";

                if(residenceCumOffice.isChecked()){
                    otherDetailsValues += residenceCumOffice.getText().toString()+",";
                }if(nonTargetArea.isChecked()){
                    otherDetailsValues += nonTargetArea.getText().toString()+",";
                }if(outsideGeographic.isChecked()){
                    otherDetailsValues += outsideGeographic.getText().toString()+",";
                }if(entryNotAllowed.isChecked()){
                    otherDetailsValues += entryNotAllowed.getText().toString()+",";
                }if(addrNotTraceable.isChecked()){
                    otherDetailsValues += addrNotTraceable.getText().toString()+",";
                }if(applicantNotWorkingHere.isChecked()){
                    otherDetailsValues += applicantNotWorkingHere.getText().toString()+",";
                }if(detailMismatch.isChecked()){
                    otherDetailsValues += detailMismatch.getText().toString()+",";
                }if(designationIncorrect.isChecked()){
                    otherDetailsValues += designationIncorrect.getText().toString()+",";
                }if(noBusinessActivity.isChecked()){
                    otherDetailsValues += noBusinessActivity.getText().toString()+",";
                }if(negativeNeighbourCheck.isChecked()){
                    otherDetailsValues += negativeNeighbourCheck.getText().toString()+",";
                }if(badMarketRepo.isChecked()){
                    otherDetailsValues += badMarketRepo.getText().toString()+",";
                }if(otherReason.isChecked()){
                    otherDetailsValues += otherReason.getText().toString()+",";
                }

                if(otherDetailsValues.endsWith(",")){
                    otherDetailsValues = otherDetailsValues.substring(0,otherDetailsValues.length() - 1);
                }
                SubmitFiOfficeAsyncTask submitFiOfficeAsyncTask = new SubmitFiOfficeAsyncTask(new FiResidentialSubmitCallBack() {
                    @Override
                    public void onSuccess(String success) {
                        Toast.makeText(OtherDetails.this, "Loading...", Toast.LENGTH_LONG).show();

                        String usr = fiDataActivityShared.getString("username","");
                        FiDataFetchAsyncTaskAfterSubmit fiDataFetchAsyncTask = new FiDataFetchAsyncTaskAfterSubmit(OtherDetails.this, usr);
                        fiDataFetchAsyncTask.execute(usr);
//                        finish();
//                        Intent i = new Intent(OtherDetails.this, FiDataActivity.class);
////                        Intent i = new Intent(OtherDetails.this, ProfileDashboardActivity.class);
//                        i.putExtras()
//                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivityForResult(i,400);

                    }
                });
                submitFiOfficeAsyncTask.execute(
                  verifiersRemark.getText().toString(), additionalIncome.getText().toString(), otherDetailsValues
                );
            }
        });
    }

    public class SubmitFiOfficeAsyncTask extends AsyncTask<String, Void, String> {
        String url = "http://www.nscs.in/api/o_submit", JSON_STRING="";
        FiResidentialSubmitCallBack fiResidentialSubmitCallBack;
        ProgressDialog progressDialog;

        SubmitFiOfficeAsyncTask(FiResidentialSubmitCallBack fiResidentialSubmitCallBack){
            this.fiResidentialSubmitCallBack = fiResidentialSubmitCallBack;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            generalDetailsShared = getSharedPreferences(generalSharedPref, Context.MODE_PRIVATE);
            salariedProfileShared = getSharedPreferences(salariedSharedPref, Context.MODE_PRIVATE);
            neighbourCheckShared = getSharedPreferences(NeighbourCheckDetailsSharedPref, Context.MODE_PRIVATE);
            selfEmployedSared = getSharedPreferences(selfEmployedDetailsSharedPref, Context.MODE_PRIVATE);
            fiDataActivityShared = getSharedPreferences(fiDataUser, Context.MODE_PRIVATE);

            progressDialog = new ProgressDialog(OtherDetails.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Please wait");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String od1 = params[0];
                String od2 = params[1];
                String otherConcerningIssues = params[2];

                String userNmae = fiDataActivityShared.getString("username","");
                String id = fiDataActivityShared.getString("caseId", "");


                URL loginURL = new URL(url);
                Log.d("loginURL","loginURL reached");
                HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();
                Log.d("httpconn","httpconn reached");

                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
//                httpURLConnection.setReadTimeout(15000);
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setAllowUserInteraction(false);

                //personalResidentDetails
                httpURLConnection.setRequestProperty("id", id);
//                httpURLConnection.setRequestProperty("id", "2");
                httpURLConnection.setRequestProperty("username",userNmae);
//                httpURLConnection.setRequestProperty("username","fe3");
                httpURLConnection.setRequestProperty("gd1",generalDetailsShared.getString("personMet",""));
                httpURLConnection.setRequestProperty("gd2",generalDetailsShared.getString("relationWithApplicant",""));
                httpURLConnection.setRequestProperty("gd3",generalDetailsShared.getString("companyName",""));
                httpURLConnection.setRequestProperty("gd4",generalDetailsShared.getString("role",""));
                httpURLConnection.setRequestProperty("gd5",generalDetailsShared.getString("phone",""));
                httpURLConnection.setRequestProperty("gd6",generalDetailsShared.getString("email",""));
                httpURLConnection.setRequestProperty("gd7",generalDetailsShared.getString("visitingCard",""));
                httpURLConnection.setRequestProperty("gd8",generalDetailsShared.getString("noOfYrsBusinessp",""));
                httpURLConnection.setRequestProperty("gd9",generalDetailsShared.getString("typeOrNatureOfBiz",""));
                httpURLConnection.setRequestProperty("gd10",generalDetailsShared.getString("productDealt",""));
                httpURLConnection.setRequestProperty("gd11",generalDetailsShared.getString("noOfEmpSeen",""));
                httpURLConnection.setRequestProperty("gd12",generalDetailsShared.getString("areaOfOffice",""));
                httpURLConnection.setRequestProperty("gd13",generalDetailsShared.getString("typeBizOfc",""));
                httpURLConnection.setRequestProperty("gd14",generalDetailsShared.getString("localityPfcBiz",""));
                httpURLConnection.setRequestProperty("gd15",generalDetailsShared.getString("commonOfficeAndResidence",""));
                httpURLConnection.setRequestProperty("gd16",generalDetailsShared.getString("enteranceAvailable",""));
                httpURLConnection.setRequestProperty("gd17",generalDetailsShared.getString("constructionOfcType",""));
                httpURLConnection.setRequestProperty("gd18",generalDetailsShared.getString("exterior",""));
                httpURLConnection.setRequestProperty("gd19",generalDetailsShared.getString("interiors",""));
                httpURLConnection.setRequestProperty("gd20",generalDetailsShared.getString("businessLevelActivities",""));
                httpURLConnection.setRequestProperty("gd21",generalDetailsShared.getString("officeSetUp",""));
                httpURLConnection.setRequestProperty("gd22",generalDetailsShared.getString("accessibility",""));
                httpURLConnection.setRequestProperty("gd23",generalDetailsShared.getString("routeMap",""));

                //salaried profile
                httpURLConnection.setRequestProperty("sp1",salariedProfileShared.getString("companyName",""));
                httpURLConnection.setRequestProperty("sp2",salariedProfileShared.getString("companyProfile",""));
                httpURLConnection.setRequestProperty("sp3",salariedProfileShared.getString("applicantDesignation",""));
                httpURLConnection.setRequestProperty("sp4",salariedProfileShared.getString("department",""));
                httpURLConnection.setRequestProperty("sp5",salariedProfileShared.getString("reportingTo",""));
                httpURLConnection.setRequestProperty("sp6",salariedProfileShared.getString("employeeNumber",""));
                httpURLConnection.setRequestProperty("sp7",salariedProfileShared.getString("applicantJob",""));
                httpURLConnection.setRequestProperty("sp8",salariedProfileShared.getString("yrsOfExperience",""));
                httpURLConnection.setRequestProperty("sp9",salariedProfileShared.getString("salaryDetails",""));
                httpURLConnection.setRequestProperty("sp10",salariedProfileShared.getString("transferableJob",""));
                httpURLConnection.setRequestProperty("sp11",salariedProfileShared.getString("totalNoOfEmployee",""));
                httpURLConnection.setRequestProperty("sp12",salariedProfileShared.getString("numberOfBranches",""));
                httpURLConnection.setRequestProperty("sp13",salariedProfileShared.getString("previousJobDetails",""));

                httpURLConnection.setRequestProperty("sep1",selfEmployedSared.getString("personMet",""));
                httpURLConnection.setRequestProperty("sep2",selfEmployedSared.getString("natureOfBusiness",""));
                httpURLConnection.setRequestProperty("sep3",selfEmployedSared.getString("businessBoardSeen",""));
                httpURLConnection.setRequestProperty("sep4",selfEmployedSared.getString("businessProof",""));
                httpURLConnection.setRequestProperty("sep5",selfEmployedSared.getString("businessOwnership",""));
                httpURLConnection.setRequestProperty("sep6",selfEmployedSared.getString("businessPremises",""));
                httpURLConnection.setRequestProperty("sep7",selfEmployedSared.getString("exteriorPremises",""));
                httpURLConnection.setRequestProperty("sep8",selfEmployedSared.getString("interiorPremises",""));
                httpURLConnection.setRequestProperty("sep9",selfEmployedSared.getString("totalNoEmployee",""));
                httpURLConnection.setRequestProperty("sep10",selfEmployedSared.getString("numberOfEmployeeSeen",""));
                httpURLConnection.setRequestProperty("sep11",selfEmployedSared.getString("numberOfCustomerSeen",""));
                httpURLConnection.setRequestProperty("sep12",selfEmployedSared.getString("avgBillingPerCustomer",""));
                httpURLConnection.setRequestProperty("sep13",selfEmployedSared.getString("numberOfCustomerPerDay",""));
                httpURLConnection.setRequestProperty("sep14",selfEmployedSared.getString("avgMonthlyTurnOver",""));
                httpURLConnection.setRequestProperty("sep15",selfEmployedSared.getString("stockAndMachinery",""));
                httpURLConnection.setRequestProperty("sep16",selfEmployedSared.getString("peakBusinessHours",""));
                httpURLConnection.setRequestProperty("sep17",selfEmployedSared.getString("marketHoliday",""));
                httpURLConnection.setRequestProperty("sep18",selfEmployedSared.getString("levelOfBusinessActivity",""));
                httpURLConnection.setRequestProperty("sep19",selfEmployedSared.getString("existingVehicleDetails",""));
                httpURLConnection.setRequestProperty("sep20",selfEmployedSared.getString("vehicleRegistrationNumber",""));
                httpURLConnection.setRequestProperty("sep21",selfEmployedSared.getString("vehicleFreeOrFinance",""));
                httpURLConnection.setRequestProperty("sep22",selfEmployedSared.getString("existingLoanOblogation",""));
                httpURLConnection.setRequestProperty("sep23",selfEmployedSared.getString("affiliationToPolitics",""));
                //NeighbourFeedback
                httpURLConnection.setRequestProperty("nc1",neighbourCheckShared.getString("neighbourOne",""));
                httpURLConnection.setRequestProperty("nc2",neighbourCheckShared.getString("neighbourTwo",""));
                //otherResidentDetails
                httpURLConnection.setRequestProperty("od1",od1);
                httpURLConnection.setRequestProperty("od2",od2);
                httpURLConnection.setRequestProperty("od3",otherConcerningIssues);



                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                //OtherDetails
//                String data = URLEncoder.encode("id","UTF-8") + "=" + URLEncoder.encode("2","UTF-8")+"&"+
                String data = URLEncoder.encode("id","UTF-8") + "=" + URLEncoder.encode(id,"UTF-8")+"&"+
//                    URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode("fe3","UTF-8")+"&"+
                    URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(fiDataActivityShared.getString("username",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd1","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("personMet",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd2","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("relationWithApplicant",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd3","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("companyName",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd4","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("role",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd5","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("phone",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd6","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("email",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd7","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("visitingCard",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd8","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("noOfYrsBusinessp",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd9","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("typeOrNatureOfBiz",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd10","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("productDealt",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd11","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("noOfEmpSeen",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd12","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("areaOfOffice",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd13","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("typeBizOfc",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd14","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("localityPfcBiz",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd15","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("commonOfficeAndResidence",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd16","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("enteranceAvailable",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd17","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("constructionOfcType",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd18","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("exterior",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd29","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("interiors",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd20","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("businessLevelActivities",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd21","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("officeSetUp",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd22","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("accessibility",""),"UTF-8")+"&"+
                    URLEncoder.encode("gd23","UTF-8") + "=" + URLEncoder.encode(generalDetailsShared.getString("routeMap",""),"UTF-8")+"&"+
                    //Salaried profile
                    URLEncoder.encode("sp1","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("companyName",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp2","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("companyProfile",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp3","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("applicantDesignation",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp4","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("department",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp5","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("reportingTo",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp6","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("employeeNumber",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp7","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("applicantJob",""),"UTF-8")+"&"+
                    URLEncoder.encode("os8","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("yrsOfExperience",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp9","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("salaryDetails",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp10","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("transferableJob",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp11","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("totalNoOfEmployee",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp12","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("numberOfBranches",""),"UTF-8")+"&"+
                    URLEncoder.encode("sp13","UTF-8") + "=" + URLEncoder.encode(salariedProfileShared.getString("previousJobDetails",""),"UTF-8")+"&"+

                    URLEncoder.encode("sep1","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("personMet",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep2","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("natureOfBusiness",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep3","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("businessBoardSeen",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep4","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("businessProof",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep5","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("businessOwnership",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep6","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("businessPremises",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep7","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("exteriorPremises",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep8","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("interiorPremises",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep9","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("totalNoEmployee",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep10","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("numberOfEmployeeSeen",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep11","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("numberOfCustomerSeen",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep12","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("avgBillingPerCustomer",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep13","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("numberOfCustomerPerDay",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep14","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("avgMonthlyTurnOver",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep15","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("stockAndMachinery",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep16","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("peakBusinessHours",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep17","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("marketHoliday",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep18","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("levelOfBusinessActivity",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep19","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("existingVehicleDetails",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep20","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("vehicleRegistrationNumber",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep21","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("vehicleFreeOrFinance",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep22","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("existingLoanOblogation",""),"UTF-8")+"&"+
                    URLEncoder.encode("sep23","UTF-8") + "=" + URLEncoder.encode(selfEmployedSared.getString("affiliationToPolitics",""),"UTF-8")+"&"+
                    //NeighbourFeedback
                    URLEncoder.encode("nc1","UTF-8") + "=" + URLEncoder.encode(neighbourCheckShared.getString("neighbourOne",""),"UTF-8")+"&"+
                    URLEncoder.encode("nc2","UTF-8") + "=" + URLEncoder.encode(neighbourCheckShared.getString("neighbourTwo",""),"UTF-8")+"&"+
                   //other details residence
                    URLEncoder.encode("od1","UTF-8") + "=" + URLEncoder.encode(od1,"UTF-8")+"&"+
                    URLEncoder.encode("od2","UTF-8") + "=" + URLEncoder.encode(od2,"UTF-8")+"&"+
                    URLEncoder.encode("od3","UTF-8") + "=" + URLEncoder.encode(otherConcerningIssues,"UTF-8");

                BW.write(data);
                BW.flush();
                BW.close();
                OS.close();

                InputStream IS = httpURLConnection.getInputStream();

                Log.d("IS","IS reached");
                BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
                Log.d("BR","BR reached");
                StringBuilder sb = new StringBuilder();

                while ((JSON_STRING = BR.readLine())!=null){
                    sb.append(JSON_STRING+"\n");
                }

                String jsn = sb.toString().trim();
                Log.d("jsn",jsn);
                BR.close();
                IS.close();

                httpURLConnection.disconnect();

                return  jsn;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            String responseStatus = "";
            try {
                JSONObject jsonObject = new JSONObject(result);
                responseStatus = jsonObject.getString("Response");
                if(responseStatus.equals("Success")){
                    Toast.makeText(OtherDetails.this,"Submitted Successfully.",Toast.LENGTH_SHORT).show();
//                    finish();

                }else{
                    Toast.makeText(OtherDetails.this,"Something went wrong",Toast.LENGTH_LONG).show();
                }

            fiResidentialSubmitCallBack.onSuccess("success");
        } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public class FiDataFetchAsyncTaskAfterSubmit extends AsyncTask<String,Void,String> {
        String json_getUrl = "";
        String username = "";
        String JSON_STRING = "";
        Context activityContext = null;
        ProgressDialog progressDialog;

        FiDataFetchAsyncTaskAfterSubmit(Context activityContext,String username){
            this.activityContext = activityContext;
            this.username = username;
        }

        @Override
        protected void onPreExecute() {
            json_getUrl = "http://www.nscs.in/api/fi_data";
            progressDialog = new ProgressDialog(activityContext);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Please Wait");
//            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {

            try {
                String username = params[0];

                URL loginURL = new URL(json_getUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) loginURL.openConnection();

                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setAllowUserInteraction(false);
                httpURLConnection.setRequestProperty("username",username);

                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter BW = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));

                String data = URLEncoder.encode("username","UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");

                BW.write(data);
                BW.flush();
                BW.close();
                OS.close();

                InputStream IS = httpURLConnection.getInputStream();

                Log.d("IS","IS reached");
                BufferedReader BR = new BufferedReader(new InputStreamReader(IS));
                Log.d("BR","BR reached");
                StringBuilder sb = new StringBuilder();

                while ((JSON_STRING = BR.readLine())!=null){
                    sb.append(JSON_STRING+"\n");
                }

                String jsn = sb.toString().trim();
                Log.d("jsn",jsn);
                BR.close();
                IS.close();

                httpURLConnection.disconnect();

                return  jsn;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();}
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
//            progressDialog.dismiss();

            String responseStatus = "";
            try {
                JSONObject jsonObject = new JSONObject(result);
                responseStatus = jsonObject.getString("Response");
                if(responseStatus.equals("Success")){
                    Intent fi = new Intent(activityContext, FiDataActivity.class);
                    fi.putExtra("fiData",result);
                    fi.putExtra("username",this.username);
                    fi.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activityContext.startActivity(fi);
                }else{
                    Toast.makeText(activityContext,"Something went wrong",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(activityContext,"Server Error",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

}
