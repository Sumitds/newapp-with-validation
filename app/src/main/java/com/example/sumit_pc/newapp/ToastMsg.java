package com.example.sumit_pc.newapp;

import android.content.Context;
import android.widget.Toast;

import com.example.sumit_pc.newapp.residenceverification.PersonalDetails;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sumit-pc on 7/1/18.
 */

public class ToastMsg {
    Context context;
    public ToastMsg(Context context){
        this.context = context;
    }

    public void showToast(String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public boolean isContainSpecialChar(String str){
        Pattern pattern = Pattern.compile("[a-zA-Z0-9 ,.]$");
        Matcher matcher = pattern.matcher(str);
        if(!matcher.matches()){
            return false;
        }else {
            return true;
        }
    }
    public boolean isContainSpecialCharOnly(String str){
        Pattern pattern = Pattern.compile("[a-zA-Z ,.]$");
        Matcher matcher = pattern.matcher(str);
        if(!matcher.matches()){
            return false;
        }else {
            return true;
        }
    }
}
