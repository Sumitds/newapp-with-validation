package com.example.sumit_pc.newapp.officeverification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.ToastMsg;
import com.example.sumit_pc.newapp.residenceverification.NeighbourFeedback;
import com.example.sumit_pc.newapp.residenceverification.OtherDetailsResidence;

public class GeneralDetailsFiSubmit extends AppCompatActivity {
    final String generalSharedPref = "GeneralDetails";
    EditText personName, relationshipWithApplicant, companyName, role, phone, email,
            noOfYrsBusinessp, productDealt, noOfEmpSeen, areaOfOffice, accessibility, routeMap;
    Spinner typeOrNatureOfBiz, typeBizOfc, localityPfcBiz, constructionOfcType, interiors,
                        businessLevelActivities, officeSetUp;
    ArrayAdapter<CharSequence> bizNatureAdapter, bizTypeAdapter, localityBizAdapter,
            constructionOfcTypeAdapter, interiorsAdapter, businessLevelActivitiesAdapter, officeSetUpAdapter;
//    RadioGroup visingCardGroup, commonOfficeAndResidenceGroup, enteranceAvailableGroup, exteriorGroup;
    RadioButton visingCardYes,visingCardNo, commonOfficeAndResidenceNo, commonOfficeAndResidenceYes
            ,enteranceAvailableYes,enteranceAvailableNo, exteriorGood, exteriorAverage,exteriorPoor;
    Button nextSalariedInfo;
    SharedPreferences sharedpreferences ;
    SharedPreferences.Editor editor ;
    String hasVisitingCard,  hasCommonOfficeAndResidence, entranceAvailable, exterior,correctEmail;

    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_details_fi_submit);

        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        personName = (EditText) findViewById(R.id.name_person_met_office_fi);
        relationshipWithApplicant = (EditText) findViewById(R.id.relationship_with_applicant);
        companyName = (EditText) findViewById(R.id.name_company_firm);
        role = (EditText) findViewById(R.id.designation);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email_id);
        noOfYrsBusinessp = (EditText) findViewById(R.id.no_yrs_biz_job);
        productDealt = (EditText) findViewById(R.id.product_dealt_fi);
        noOfEmpSeen = (EditText) findViewById(R.id.no_of_emp_seen);
        areaOfOffice = (EditText) findViewById(R.id.area_of_office);
        accessibility = (EditText) findViewById(R.id.accessibility_office);
        routeMap = (EditText) findViewById(R.id.route_map_office);

        visingCardYes = (RadioButton) findViewById(R.id.visiting_card_yes);
        visingCardNo = (RadioButton) findViewById(R.id.visiting_card_no);

        commonOfficeAndResidenceYes = (RadioButton) findViewById(R.id.common_ofc_or_residence_yes);
        commonOfficeAndResidenceNo = (RadioButton) findViewById(R.id.common_ofc_or_residence_no);

        enteranceAvailableYes = (RadioButton) findViewById(R.id.enterance_available_yes);
        enteranceAvailableNo = (RadioButton) findViewById(R.id.enterance_available_no);

        exteriorGood = (RadioButton) findViewById(R.id.exteriors_good);
        exteriorAverage = (RadioButton) findViewById(R.id.exteriors_average);
        exteriorPoor = (RadioButton) findViewById(R.id.exteriors_poor);

        nextSalariedInfo = (Button) findViewById(R.id.next_salaried_profile);

        typeOrNatureOfBiz = (Spinner) findViewById(R.id.type_or_nature_of_biz);
        bizNatureAdapter = ArrayAdapter.createFromResource(this,
                R.array.type_or_nature_biz, android.R.layout.simple_spinner_item);
        bizNatureAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeOrNatureOfBiz.setAdapter(bizNatureAdapter);

        typeBizOfc = (Spinner) findViewById(R.id.type_of_ofc_or_biz);
        bizTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.type_of_biz_or_ofc, android.R.layout.simple_spinner_item);
        bizTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeBizOfc.setAdapter(bizTypeAdapter);

        localityPfcBiz = (Spinner) findViewById(R.id.locality_ofc_or_biz);
        localityBizAdapter = ArrayAdapter.createFromResource(this,
                R.array.locality_ofc_biz, android.R.layout.simple_spinner_item);
        bizTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        localityPfcBiz.setAdapter(localityBizAdapter);

        constructionOfcType = (Spinner) findViewById(R.id.construction_of_ofc);
        constructionOfcTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.construction_ofc_type, android.R.layout.simple_spinner_item);
        constructionOfcTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        constructionOfcType.setAdapter(constructionOfcTypeAdapter);

        interiors = (Spinner) findViewById(R.id.interiors);
        interiorsAdapter = ArrayAdapter.createFromResource(this,
                R.array.interiors_options, android.R.layout.simple_spinner_item);
        interiorsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        interiors.setAdapter(interiorsAdapter);

        businessLevelActivities = (Spinner) findViewById(R.id.business_level_activities);
        businessLevelActivitiesAdapter = ArrayAdapter.createFromResource(this,
                R.array.biz_level_activities, android.R.layout.simple_spinner_item);
        businessLevelActivitiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        businessLevelActivities.setAdapter(businessLevelActivitiesAdapter);

        officeSetUp = (Spinner) findViewById(R.id.office_set_up);
        officeSetUpAdapter = ArrayAdapter.createFromResource(this,
                R.array.ofc_setup_number, android.R.layout.simple_spinner_item);
        officeSetUpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        officeSetUp.setAdapter(officeSetUpAdapter);

        sharedpreferences = getSharedPreferences(generalSharedPref, Context.MODE_PRIVATE);

        nextSalariedInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(personName.getText().toString().equals("") || relationshipWithApplicant.getText().toString().equals("")
                        || companyName.getText().toString().equals("") || role.getText().toString().equals("")
                        || phone.getText().toString().equals("") || email.getText().toString().equals("")
                   ){
                    ToastMsg toastMsg = new ToastMsg(GeneralDetailsFiSubmit.this);
                    toastMsg.showToast("Fields with (*) mark are mandatory.");
                }else if(phone.getText().toString().length() != 10){
                    ToastMsg toastMsg = new ToastMsg(GeneralDetailsFiSubmit.this);
                    toastMsg.showToast("Enter 10 digit Phone Number");
                }else if(!email.getText().toString().contains("@") || !email.getText().toString().contains(".")){
                    ToastMsg toastMsg = new ToastMsg(GeneralDetailsFiSubmit.this);
                    toastMsg.showToast("Invalid Email Type");
                }else if(!visingCardYes.isChecked() && !visingCardNo.isChecked()){
                    ToastMsg toastMsg = new ToastMsg(GeneralDetailsFiSubmit.this);
                    toastMsg.showToast("Please select an option for visiting card.");
                }else{
                    correctEmail = email.getText().toString();
                    if (visingCardYes.isChecked() || visingCardNo.isChecked()) {
                        if (visingCardYes.isChecked()) {
                            hasVisitingCard = visingCardYes.getText().toString();
                        } else if (visingCardNo.isChecked()) {
                            hasVisitingCard = visingCardNo.getText().toString();
                        }
                    }

                    ToastMsg toastMsg = new ToastMsg(GeneralDetailsFiSubmit.this);
                    if(toastMsg.isContainSpecialCharOnly(personName.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(relationshipWithApplicant.getText().toString())
                            && toastMsg.isContainSpecialCharOnly(role.getText().toString())){
                        storeGeneralDetails();
                        Intent nextActivity = new Intent(GeneralDetailsFiSubmit.this, SalariedProfileFiSubmit.class);
                        startActivity(nextActivity);
                    }else {
                        toastMsg.showToast("No special character or emoticon allowed.");

                    }

                }

            }
        });
    }

    void storeGeneralDetails(){
        String correctEmail = "";
        editor = sharedpreferences.edit();
        editor.clear();

            editor.putString("personMet",personName.getText().toString());
            editor.putString("relationWithApplicant",relationshipWithApplicant.getText().toString());
            editor.putString("companyName",companyName.getText().toString());
            editor.putString("role",role.getText().toString());
            editor.putString("phone",phone.getText().toString());
            editor.putString("email",correctEmail);
            editor.putString("visitingCard",hasVisitingCard);
            editor.putString("noOfYrsBusinessp",noOfYrsBusinessp.getText().toString());
            editor.putString("typeOrNatureOfBiz",typeOrNatureOfBiz.getSelectedItem().toString());
            editor.putString("productDealt",productDealt.getText().toString());
            editor.putString("noOfEmpSeen",noOfEmpSeen.getText().toString());
            editor.putString("areaOfOffice",areaOfOffice.getText().toString());
            editor.putString("typeBizOfc",typeBizOfc.getSelectedItem().toString());
            editor.putString("localityPfcBiz",localityPfcBiz.getSelectedItem().toString());
            editor.putString("commonOfficeAndResidence",hasCommonOfficeAndResidence);
            editor.putString("enteranceAvailable",entranceAvailable);
            editor.putString("constructionOfcType",constructionOfcType.getSelectedItem().toString());
            editor.putString("exterior",exterior);
            editor.putString("interiors",interiors.getSelectedItem().toString());
            editor.putString("businessLevelActivities",businessLevelActivities.getSelectedItem().toString());
            editor.putString("officeSetUp",officeSetUp.getSelectedItem().toString());
            editor.putString("accessibility",accessibility.getText().toString());
            editor.putString("routeMap",routeMap.getText().toString());
            editor.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
