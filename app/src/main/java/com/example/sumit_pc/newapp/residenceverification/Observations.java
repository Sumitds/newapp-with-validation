package com.example.sumit_pc.newapp.residenceverification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sumit_pc.newapp.R;
import com.example.sumit_pc.newapp.ToastMsg;

public class Observations extends AppCompatActivity {

    EditText areaInSqft, societyBoardSeen, cityWithinLimit, residenceEnteranceMotorable, prominentLandmark, namePlate,carPark,
            numberOfRoomAndApproxSize,politicalPartyAffiliation, routeMap;

    Spinner typeOfResidence, locality, accessibilityAndDistance, constructionOfHouse, furnishingDetails,
            residenceExteriorDetails, typeOfFlooring, typeOfRoofing, assetSeen, standardLiving;
    ArrayAdapter<CharSequence>  typeOfResidenceAdapter, localityAdapter, accessibilityAndDistanceAdapter,
            constructionOfHouseAdapter, furnishingDetailsAdapter,
            residenceExteriorDetailsAdapter, typeOfFlooringAdapter,
            typeOfRoofingAdapter, assetSeenAdapter, standardLivingAdapter;
    ActionBar actionBar;
    Button neighbourFeedBack;
    RadioGroup entryPermitted;
    RadioButton entryPermittedYes, entryPermittedNo;
    final String observationsSharePreference = "Observations";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String isEntryPermitted="not initialized";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observations);


        actionBar=  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        areaInSqft = (EditText) findViewById(R.id.area_observations);
        societyBoardSeen = (EditText) findViewById(R.id.society_board_seen);
        cityWithinLimit = (EditText) findViewById(R.id.within_city_limit_observation);
        residenceEnteranceMotorable = (EditText) findViewById(R.id.residence_motorable_enterence);
        prominentLandmark = (EditText) findViewById(R.id.prominent_landmark);
        namePlate = (EditText) findViewById(R.id.name_plate);
        carPark = (EditText) findViewById(R.id.car_park);
        numberOfRoomAndApproxSize = (EditText) findViewById(R.id.room_approx_size);
        politicalPartyAffiliation = (EditText) findViewById(R.id.any_political_party_affiliation_seen);
        routeMap = (EditText) findViewById(R.id.route_map_observations);
        entryPermitted = (RadioGroup) findViewById(R.id.entry_permitted);
        entryPermittedYes = (RadioButton) findViewById(R.id.entry_permitted_yes);
        entryPermittedNo = (RadioButton) findViewById(R.id.entry_permitted_no);

        typeOfResidence = (Spinner) findViewById(R.id.type_of_residence);
        typeOfResidenceAdapter = ArrayAdapter.createFromResource(this,
                R.array.type_of_residence_options, android.R.layout.simple_spinner_item);
        typeOfResidenceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeOfResidence.setAdapter(typeOfResidenceAdapter);

        locality = (Spinner) findViewById(R.id.locality_observations);
        localityAdapter = ArrayAdapter.createFromResource(this,
                R.array.locality_observation_options, android.R.layout.simple_spinner_item);
        localityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locality.setAdapter(localityAdapter);

        accessibilityAndDistance = (Spinner) findViewById(R.id.accessibility_and_distance);
        accessibilityAndDistanceAdapter = ArrayAdapter.createFromResource(this,
                R.array.accessibility_and_distance_options, android.R.layout.simple_spinner_item);
        accessibilityAndDistanceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        accessibilityAndDistance.setAdapter(accessibilityAndDistanceAdapter);

        constructionOfHouse = (Spinner) findViewById(R.id.construction_of_house);
        constructionOfHouseAdapter = ArrayAdapter.createFromResource(this,
                R.array.construction_ofc_type, android.R.layout.simple_spinner_item);
        constructionOfHouseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        constructionOfHouse.setAdapter(constructionOfHouseAdapter);

        furnishingDetails = (Spinner) findViewById(R.id.furnishing_details_observation);
        furnishingDetailsAdapter = ArrayAdapter.createFromResource(this,
                R.array.furnishing_details_options, android.R.layout.simple_spinner_item);
        furnishingDetailsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        furnishingDetails.setAdapter(furnishingDetailsAdapter);

        residenceExteriorDetails = (Spinner) findViewById(R.id.residence_exterior_details);
        residenceExteriorDetailsAdapter = ArrayAdapter.createFromResource(this,
                R.array.residemce_exterior_options, android.R.layout.simple_spinner_item);
        residenceExteriorDetailsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        residenceExteriorDetails.setAdapter(residenceExteriorDetailsAdapter);

        typeOfFlooring = (Spinner) findViewById(R.id.type_of_flooring_observation);
        typeOfFlooringAdapter = ArrayAdapter.createFromResource(this,
                R.array.flooring_type_options, android.R.layout.simple_spinner_item);
        typeOfFlooringAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeOfFlooring.setAdapter(typeOfResidenceAdapter);

        typeOfRoofing = (Spinner) findViewById(R.id.type_of_roofing_observation);
        typeOfRoofingAdapter = ArrayAdapter.createFromResource(this,
                R.array.roofing_type_options, android.R.layout.simple_spinner_item);
        typeOfRoofingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeOfRoofing.setAdapter(typeOfRoofingAdapter);

        assetSeen = (Spinner) findViewById(R.id.assets_seen_observation);
        assetSeenAdapter = ArrayAdapter.createFromResource(this,
                R.array.assets_seen_options, android.R.layout.simple_spinner_item);
        assetSeenAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        assetSeen.setAdapter(assetSeenAdapter);

        standardLiving = (Spinner) findViewById(R.id.standard_of_living);
        standardLivingAdapter = ArrayAdapter.createFromResource(this,
                R.array.standard_living_options, android.R.layout.simple_spinner_item);
        standardLivingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        standardLiving.setAdapter(standardLivingAdapter);

        sharedPreferences = getSharedPreferences(observationsSharePreference, Context.MODE_PRIVATE);
        neighbourFeedBack = (Button) findViewById(R.id.next_neighbour_check_observation);

        neighbourFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            ToastMsg toastMsg = new ToastMsg(Observations.this);
            if(entryPermittedYes.isChecked() || entryPermittedNo.isChecked()){

                if(entryPermittedYes.isChecked()){
                    isEntryPermitted = entryPermittedYes.getText().toString();
                }else if(entryPermittedNo.isChecked()){
                    isEntryPermitted = entryPermittedNo.getText().toString();
                }
                if((toastMsg.isContainSpecialChar(societyBoardSeen.getText().toString()))
                    && (toastMsg.isContainSpecialChar(cityWithinLimit.getText().toString()))
                    && (toastMsg.isContainSpecialChar(residenceEnteranceMotorable.getText().toString()))
                    && (toastMsg.isContainSpecialChar(prominentLandmark.getText().toString()))
                    && (toastMsg.isContainSpecialChar(namePlate.getText().toString()))
                    && (toastMsg.isContainSpecialChar(carPark.getText().toString()))
                    && (toastMsg.isContainSpecialChar(politicalPartyAffiliation.getText().toString()))
                    && (toastMsg.isContainSpecialChar(routeMap.getText().toString()))){
                    toastMsg.showToast("No special character or emoticon allowed.");
                }else {
                storeObservationDetails();
                Intent nextNeighbourFeedback = new Intent(Observations.this, NeighbourFeedback.class);
                nextNeighbourFeedback.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(nextNeighbourFeedback);
                }
            }else if(!entryPermittedYes.isChecked() || !entryPermittedNo.isChecked()){
                isEntryPermitted = "";
                if(!(toastMsg.isContainSpecialChar(societyBoardSeen.getText().toString()))
                        && !(toastMsg.isContainSpecialChar(cityWithinLimit.getText().toString()))
                        && !(toastMsg.isContainSpecialChar(residenceEnteranceMotorable.getText().toString()))
                        && !(toastMsg.isContainSpecialChar(prominentLandmark.getText().toString()))
                        && !(toastMsg.isContainSpecialChar(namePlate.getText().toString()))
                        && !(toastMsg.isContainSpecialChar(carPark.getText().toString()))
                        && !(toastMsg.isContainSpecialChar(politicalPartyAffiliation.getText().toString()))
                        && !(toastMsg.isContainSpecialChar(routeMap.getText().toString()))){
                    storeObservationDetails();
                    Intent nextNeighbourFeedback = new Intent(Observations.this, NeighbourFeedback.class);
                    nextNeighbourFeedback.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(nextNeighbourFeedback);
                }else {
                    toastMsg.showToast("No special character or emoticon allowed.");

                }
            }
            }
        });

    }

    void storeObservationDetails(){

        editor = sharedPreferences.edit();
        editor.clear();
        editor.putString("areaInSqft",areaInSqft.getText().toString());
        editor.putString("societyBoardSeen",societyBoardSeen.getText().toString());
        editor.putString("cityWithinLimit",cityWithinLimit.getText().toString());
        editor.putString("residenceEnteranceMotorable",residenceEnteranceMotorable.getText().toString());
        editor.putString("prominentLandmark",prominentLandmark.getText().toString());
        editor.putString("namePlate",namePlate.getText().toString());
        editor.putString("carPark",carPark.getText().toString());
        editor.putString("numberOfRoomAndApproxSize",numberOfRoomAndApproxSize.getText().toString());
        editor.putString("politicalPartyAffiliation",politicalPartyAffiliation.getText().toString());
        editor.putString("routeMap",routeMap.getText().toString());
        editor.putString("entryPermitted",isEntryPermitted);
        editor.putString("typeOfResidence",typeOfResidence.getSelectedItem().toString());
        editor.putString("locality",locality.getSelectedItem().toString());
        editor.putString("accessibilityAndDistance",accessibilityAndDistance.getSelectedItem().toString());
        editor.putString("constructionOfHouse",constructionOfHouse.getSelectedItem().toString());
        editor.putString("furnishingDetails",furnishingDetails.getSelectedItem().toString());
        editor.putString("residenceExteriorDetails",residenceExteriorDetails.getSelectedItem().toString());
        editor.putString("typeOfFlooring",typeOfFlooring.getSelectedItem().toString());
        editor.putString("typeOfRoofing",typeOfRoofing.getSelectedItem().toString());
        editor.putString("assetSeen",assetSeen.getSelectedItem().toString());
        editor.putString("standardLiving",standardLiving.getSelectedItem().toString());
        editor.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
